/**
 * @file voevent.h
 * @brief Header file for the voevent reader/parser.
 *
 * Here are declared all the global variables, constants and the
 * new structures defined.
 */

#ifndef VOEVENT_H
#define VOEVENT_H

#include <stdio.h>
#include <time.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

extern const char *VOEventSchemaFile;
extern const char *TransportSchemaFile;

/// Number of possible VOEvent 'role' attribute values
#define MAX_ROLES 4
/// Number of possible VOEvent AstroCoord systems values
#define MAX_ASTRO_COORD_SYSTEMS 18
/// Number of possible VOEvent general observatory locations ID
#define MAX_OBSERVATORY_LOCATIONS 5
/// Number of possible VOEvent 'cite' attribute values
#define MAX_CITES 3

/// Possible VOEvent 'role' attribute values
extern const char* roles[MAX_ROLES];
/// Possible VOEvent AstroCoord systems values
extern const char* astro_coord_systems[MAX_ASTRO_COORD_SYSTEMS];
/// Possible VOEvent general observatory locations ID
extern const char* observatory_locations[MAX_OBSERVATORY_LOCATIONS];
/// Possible VOEvent 'cite' attribute values
extern const char* cites[MAX_CITES];

/**
 * @enum roles_values
 * @brief      Enum containing the possible VOEvent roles
 */
enum roles_values {OBSERVATION=0, PREDICTION, UTILITY, TEST};

/**
 * @enum astro_coord_systems_values
 * @brief      Enum containing the time-coordinate systems
 */
enum astro_coord_systems_values {
    TT_ICRS_TOPO=0, UTC_ICRS_TOPO, TT_FK5_TOPO, UTC_FK5_TOPO, GPS_ICRS_TOPO,
    GPS_FK5_TOPO, TT_ICRS_GEO, UTC_ICRS_GEO, TT_FK5_GEO, UTC_FK5_GEO,
    GPS_ICRS_GEO, GPS_FK5_GEO, TDB_ICRS_BARY, TDB_FK5_BARY, UTC_HPC_TOPO,
    UTC_HPR_TOPO, UTC_HGS_TOPO, UTC_HGC_TOPO};

/**
 * @enum observatory_locations_values
 * @brief      Enum containing general observatory locations ID
 */
enum observatory_locations_values {GEOSURFACE=0, GEOLEO, GEOGSO, GEONBH, GEOLUN};

/**
 * @enum cites_values
 * @brief      Enum containing the possible cite attribute values
 */
enum cites_values {FOLLOWUP=0, SUPERSEDES, RETRACTION};

/**
 * @brief      A structure containing a Reference element
 */
struct VOEvent_Reference
{
    xmlChar *uri;           ///< URI (identifier of another document)
    xmlChar *type;          ///< Type of document
    xmlChar *mimetype;      ///< Optional MIME type of the referenced document
    xmlChar *meaning;       ///< The nature of the referenced document
};

/**
 * @brief       A structure containing a Description element
 */
struct VOEvent_Description
{
    xmlChar *description;   ///< The actual description
};

/**
 * @brief       A structure containing the VOEvent root element
 */
struct VOEvent_root
{
    xmlChar *version;       ///< The version of the VOEvent
    xmlChar *ivorn;         ///< The VOEvent IVORN (unique identifier)
    xmlChar *role;          ///< Role attribute (possible values: observation, prediction, utility, test)
};

/**
 * @brief       A structure containing an Author element
 */
struct VOEvent_Author
{
    xmlChar *title;         ///< The Author title
    xmlChar *shortName;     ///< Short name of the Author
    xmlChar *logoURL;       ///< URL for the Author
    xmlChar *contactName;   ///< Contact name of the Author
    xmlChar *contactEmail;  ///< Contact email of the Author
    xmlChar *contactPhone;  ///< Contact phone of the Author
    xmlChar *contributor;   ///< Contributor of the Author
};

// Who element:
//    * Description: at most one
//    * Reference: at most one
//    * Author: at most one
//    * Date: at most one
//    * AuthorIVORN: at most one

/**
 * @brief       A structure containing a Who element
 */
struct VOEvent_Who
{
    xmlChar *AuthorIVORN;                        ///< The Author IVORN
    xmlChar *Date;                               ///< The Date of the VOEvent
    struct VOEvent_Reference *Reference;         ///< A Reference element
    struct VOEvent_Description *Description;     ///< A Description element
    struct VOEvent_Author *Author;               ///< An Author element
};

// Param element:
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a Param element
 */
struct VOEvent_Param
{
    struct VOEvent_Description *Description;     ///< A Description element
    struct VOEvent_Reference *Reference;         ///< A Reference element
    int nr_description;                          ///< Number of Description elements
    int nr_reference;                            ///< Number of Reference elements
    xmlChar *name;                               ///< Name of the parameter
    xmlChar *ucd;                                ///< UCD of the parameter
    xmlChar *value;                              ///< Value of the parameter
    xmlChar *unit;                               ///< Unit of the parameter value
    xmlChar *dataType;                           ///< Type of the data of the parameter
    xmlChar *utype;                              ///< Utype of the parameter
    xmlChar *Value;                              ///< Value element of the parameter
};

// Group element:
//    * Param: unbounded
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a Group element
 */
struct VOEvent_Group
{
    struct VOEvent_Param *Param;                 ///< A Param element
    struct VOEvent_Description *Description;     ///< A Description element
    struct VOEvent_Reference *Reference;         ///< A Reference element
    int nr_params;                               ///< Number of Param elements
    int nr_description;                          ///< Number of Description elements
    int nr_reference;                            ///< Number of Reference elements
    xmlChar *name;                               ///< Name of the group
    xmlChar *type;                               ///< Type of the group
};

// Table element:
//    * Description: unbounded
//    * Reference: unbounded
//    * Param: unbounded
//    * Field: unbounded
//    * Data: 1

/**
 * @brief       A structure containing a Table element
 */
struct VOEvent_Table
{
    struct VOEvent_Description *Description;       ///< A Description element
    struct VOEvent_Reference *Reference;           ///< A Reference element
    struct VOEvent_Param *Param;                   ///< A Param element
    struct VOEvent_Field *Field;                   ///< A Field element
    struct VOEvent_Data *Data;                     ///< A Data element
    xmlChar *name;                                 ///< Name of the table
    xmlChar *type;                                 ///< Type of the table
    int nr_description;                            ///< Number of Description elements
    int nr_reference;                              ///< Number of Reference elements
    int nr_params;                                 ///< Number of Param elements
    int nr_columns;                                ///< Number of columns of the table
    int nr_rows;                                   ///< Number of rows of the table
};

// Field element:
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a Field element
 */
struct VOEvent_Field
{
    struct VOEvent_Description *Description;        ///< A Description element
    struct VOEvent_Reference *Reference;            ///< A Reference element
    int nr_description;                             ///< Number of Description elements
    int nr_reference;                               ///< Number of Reference elements
    xmlChar *name;                                  ///< Name of the Field
    xmlChar *ucd;                                   ///< UCD of the Field
    xmlChar *unit;                                  ///< Unit of the Field
    xmlChar *dataType;                              ///< Datatype of the Field
    xmlChar *utype;                                 ///< Utype of the Field
};

/**
 * @brief       A structure containing a Data element
 */
struct VOEvent_Data
{
    xmlChar *data; ///< array containing the Table elements content
};

// What element:
//    * Param: unbounded
//    * Group: unbounded
//    * Table: unbounded
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a What element
 */
struct VOEvent_What
{
    struct VOEvent_Param *Param;                       ///< A Param element
    struct VOEvent_Group *Group;                       ///< A Group element
    struct VOEvent_Table *Table;                       ///< A Table element
    struct VOEvent_Description *Description;           ///< A Description element
    struct VOEvent_Reference *Reference;               ///< A Reference element
    int nr_description;                                ///< Number of Description elements
    int nr_reference;                                  ///< Number of Reference elements
    int nr_params;                                     ///< Number of Param elements
    int nr_groups;                                     ///< Number of Group elements
    int nr_tables;                                     ///< Number of Table elements
};

// How element:
//    * Description: unbounded number
//    * Reference:   unbounded number

/**
 * @brief       A structure containing a How element
 */
struct VOEvent_How
{
    struct VOEvent_Description *Description;           ///< A Description element
    struct VOEvent_Reference *Reference;               ///< A Reference element
    int nr_description;                                ///< Number of Description elements
    int nr_reference;                                  ///< Number of Reference elements
};

/**
 * @brief       A structure containing a EventIVORN element
 */
struct VOEvent_EventIVORN
{
    xmlChar *ivorn;      ///< IVORN value
    xmlChar *cite;       ///< Cite value (possible values: followup, supersedes, retraction)
};

// Citations element:
//    * EventIVORN: unlimited number
//    * Description: at most one

/**
 * @brief       A structure containing a Citation element
 */
struct VOEvent_Citations
{
    struct VOEvent_EventIVORN *EventIVORN;             ///< A EventIVORN element
    struct VOEvent_Description *Description;           ///< A Description element
    int nr_event_ivorn;                                ///< Number of EventIVORN elements
};

// WhereWhen element
//    * ObsDataLocation: 1
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a WhereWhen element
 */
struct VOEvent_WhereWhen
{
    //xmlNodePtr node;
    struct VOEvent_ObsDataLocation *ObsDataLocation;   ///< A ObsDataLcoation element
    struct VOEvent_Description *Description;           ///< A Description element
    struct VOEvent_Reference *Reference;               ///< A Reference element
    xmlChar *id;                                       ///< Id
};

// ObsDataLocation element
//    * ObservatoryLocation: 1
//    * ObservationLocation: 1

/**
 * @brief       A structure containing a ObsDataLocation element
 */
struct VOEvent_ObsDataLocation
{
    //xmlNodePtr node;
    struct VOEvent_ObservatoryLocation *ObservatoryLocation;    ///< A ObservatoryLocation element
    struct VOEvent_ObservationLocation *ObservationLocation;    ///< A ObservationLocation element
};

// ObservationLocation element
//    * AstroCoordSystem: 1
//    * AstroCoords: 1

/**
 * @brief       A structure containing a ObservationLocation element
 */
struct VOEvent_ObservationLocation
{
    //xmlNodePtr node;
    xmlChar *AstroCoordSystem;                            ///< The astronomical coordinates system
    struct VOEvent_AstroCoords *AstroCoords;              ///< A AstroCoords element
};

// AstroCoords element
//    * Time: at most one
//    * Position2D: at most one
//    * Position3D: at most one

/**
 * @brief       A structure containing a AstroCoords element
 */
struct VOEvent_AstroCoords
{
    struct VOEvent_Time *Time;                            ///< A Time element
    struct VOEvent_Position2D *Position2D;                ///< A Position2D element
    struct VOEvent_Position3D *Position3D;                ///< A Position3D element
    xmlChar *coord_system_id;                             ///< Coordinate system ID
};

// Time element
//    * TimeInstant: unbounded
//    * Error: unbounded

/**
 * @brief       A structure containing a Time element
 */
struct VOEvent_Time
{
    struct VOEvent_TimeInstant *TimeInstant;              ///< A TimeInstant element
    xmlChar *Error;                                       ///< Error on the time value
};

// TimeInstant element
//    * ISOTime: up to 1
//    * TimeOffset: up to 1
//    * TimeScale: up to 1

/**
 * @brief       A structure containing a TimeInstant element
 */
struct VOEvent_TimeInstant
{
    xmlChar *ISOTime;                                     ///< The time value
    xmlChar *TimeOffset;                                  ///< The time offset
    xmlChar *TimeScale;                                   ///< The timescale
};

/**
 * @brief       A structure containing a Position2D element
 */
struct VOEvent_Position2D
{
    xmlChar *Name1;                                       ///< Name of first value
    xmlChar *Name2;                                       ///< Name of second value
    xmlChar *C1;                                          ///< First value
    xmlChar *C2;                                          ///< Second value
    xmlChar *ErrorRadius;                                 ///< Error on the coordinates
    xmlChar *unit;                                        ///< Unit for the coordinates values
};

/**
 * @brief       A structure containing a Position3D element
 */
struct VOEvent_Position3D
{
    xmlChar *Name1;                                       ///< Name of first value
    xmlChar *Name2;                                       ///< Name of second value
    xmlChar *Name3;                                       ///< Name of third value
    xmlChar *C1;                                          ///< First value
    xmlChar *C2;                                          ///< Second value
    xmlChar *C3;                                          ///< Third value
    xmlChar *unit;                                        ///< Unit for the coordinates values
};

// ObservatoryLocation element
//    * AstroCoordSystem: 1
//    * AstroCoords: 1

/**
 * @brief       A structure containing a ObservatoryLocation element
 */
struct VOEvent_ObservatoryLocation
{
    xmlChar *AstroCoordSystem;                            ///< Astronomical Coordinates System
    xmlChar *id;                                          ///< ID
    struct VOEvent_AstroCoords *AstroCoords;              ///< An AstroCoords element
};

/**
 * @brief       A structure containing a Name element
 */
struct VOEvent_Name
{
    xmlChar *name;                                        ///< Name
};

/**
 * @brief       A structure containing a Concept element
 */
struct VOEvent_Concept
{
    xmlChar *concept;                                     ///< Content of the concept element
};

// Inference element:
//    * Name: unbounded
//    * Concept: unbounded
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a Inference element
 */
struct VOEvent_Inference
{
    struct VOEvent_Name *Name;                            ///< A Name element
    struct VOEvent_Concept *Concept;                      ///< A Concept element
    struct VOEvent_Description *Description;              ///< A Description element
    struct VOEvent_Reference *Reference;                  ///< A Reference element
    int nr_name;                                          ///< Number of Name elements
    int nr_concept;                                       ///< Number of Concept elements
    int nr_description;                                   ///< Number of Description elements
    int nr_reference;                                     ///< Number of Reference elements
    xmlChar *probability;                                 ///< Probability value
    xmlChar *relation;                                    ///< Relation value
};

// Why element:
//    * Name: unbounded
//    * Concept: unbounded
//    * Inference: unbounded
//    * Description: unbounded
//    * Reference: unbounded

/**
 * @brief       A structure containing a Why element
 */
struct VOEvent_Why
{
    //xmlNodePtr node;
    struct VOEvent_Name *Name;                            ///< A Name element
    struct VOEvent_Concept *Concept;                      ///< A Concept element
    struct VOEvent_Inference *Inference;                  ///< A Inference element
    struct VOEvent_Description *Description;              ///< A Description element
    struct VOEvent_Reference *Reference;                  ///< A Reference element
    int nr_name;                                          ///< Number of Name elements
    int nr_concept;                                       ///< Number of Concept elements
    int nr_inference;                                     ///< Number of Inference elements
    int nr_description;                                   ///< Number of Description elements
    int nr_reference;                                     ///< Number of Reference elements
    xmlChar *importance;                                  ///< Importance value
    xmlChar *expires;                                     ///< Expire time of the information
};

/**
 * @brief      A structure containing the VOEvent tree
 */
struct VOEvent
{
    xmlDocPtr Doc;                                        ///< An XML pointer to the document tree
    struct VOEvent_root *VOEvent;                         ///< A VOEvent root element
    struct VOEvent_Who *Who;                              ///< A Who element
    struct VOEvent_What *What;                            ///< A What element
    struct VOEvent_WhereWhen *WhereWhen;                  ///< A WhereWhen element
    struct VOEvent_How *How;                              ///< A How element
    struct VOEvent_Why *Why;                              ///< A Why element
    struct VOEvent_Citations *Citations;                  ///< A Citations element
    struct VOEvent_Description *Description;              ///< A Description element
    struct VOEvent_Reference *Reference;                  ///< A Reference element
};

/**
 * @brief       A structure containing the Transport root element
 */
struct Transport_root
{
    xmlChar *version;       ///< The version of the Transport message
    xmlChar *role;          ///< Role attribute (possible values: iamalive, authenticate, ack, nak)
};

/**
 * @brief       A structure containing the Transport Meta element
 */
struct Transport_Meta
{
    struct VOEvent_Param *meta_param;       ///< Meta information
    xmlChar *result;                 ///< Gives more details on the message
    int nr_params;                   ///< Number of Param elements
};

/**
 * @brief      A structure containing the Transport tree
 */
struct Transport
{
    xmlDocPtr Doc;                      ///< An XML pointer to the document tree
    struct Transport_root *Transport;   ///< Transport message root element               
    xmlChar *Origin;                    ///< Origin of the transport message                  
    xmlChar *TimeStamp;                 ///< Timestamp of the transport message
    xmlChar *Response;                  ///< Contains URI of the subscriber
    struct Transport_Meta *Meta;        ///< Meta information element
};

/**
 * @brief      A structure containing a UCD word and its description
 *
 * See http://www.ivoa.net/documents/UCD1+/20200212/PEN-UCDlist-1.4-20200212.pdf
 * for a detailed description of UCD words.
 *
 */
struct ucdlist {
    char *ucd_word;           ///< Name of the UCD word
    char *ucd_description;    ///< Description of the UCD word
};

int validate_voevent_xmlfile(const char *, const char *);
int validate_voevent_xmlmemory(const char *, const long int, const char *);
xmlXPathObjectPtr getnodeset(xmlDocPtr, xmlChar *);

// parsing functions
void parse_root(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_root *);
void parse_reference(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Reference *);
void parse_author(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Author *);
void parse_who(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Who *);
void parse_what(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_What *);
void parse_how(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_How *);
void parse_wherewhen(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_WhereWhen *);
void parse_why(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Why *);
void parse_param(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Param *);
void parse_group(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Group *);
void parse_param_value(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Param *);
void parse_field(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Field *);
void parse_data(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Data *);
void parse_table(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Table *);
void parse_obsdatalocation(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_ObsDataLocation *);
void parse_observatorylocation(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_ObservatoryLocation *);
void parse_observationlocation(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_ObservationLocation *);
void parse_astrocoords(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_AstroCoords *);
void parse_time(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Time *);
void parse_timeinstant(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_TimeInstant *);
void parse_position2d(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Position2D *);
void parse_position3d(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Position3D *);
void parse_citations(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Citations *);
void parse_description(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct VOEvent_Description *);
void parse_event_ivorn(xmlDocPtr, xmlNsPtr, struct VOEvent_Citations *);
void parse_inference(xmlDocPtr, xmlNsPtr, xmlNodeSetPtr, struct VOEvent_Inference *);
int parse_voevent_xmlfile(const char *, struct VOEvent *);
int parse_voevent_xmlmemory(const char *, const long int, struct VOEvent *);

int parse_transport_xmlfile(const char *, struct Transport *);
int parse_transport_xmlmemory(const char *, const long int, struct Transport *);
void parse_root_transport(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct Transport_root *);
void parse_meta_transport(xmlDocPtr, xmlNsPtr, xmlNodePtr, struct Transport_Meta *);
void free_transport(struct Transport *);
void free_transport_root(struct Transport *);
void free_transport_meta(struct Transport *);
double get_transport_sod(struct Transport *);

// writing functions
xmlDocPtr create_doc(void);
xmlNodePtr create_root_node(xmlDocPtr, const char *, const int);
xmlNodePtr create_who(xmlNodePtr, const struct tm*, const char*, const struct VOEvent_Description*, const struct VOEvent_Reference*);
xmlNodePtr create_what(xmlNodePtr, const struct VOEvent_Description*, const struct VOEvent_Reference*);
xmlNodePtr create_wherewhen(xmlNodePtr, const char *);
xmlNodePtr create_how(
    xmlNodePtr, const struct VOEvent_Reference*,
    const struct VOEvent_Description*);
xmlNodePtr create_why(
    xmlNodePtr, const float, const struct tm *,
    const struct VOEvent_Inference*);
xmlNodePtr create_citations(
    xmlNodePtr, const char*, const int,
    const struct VOEvent_Description*);
int add_author(xmlNodePtr who_node, const char*, const char*,const char*, const char*, const char*, const char*, const char*);
int fill_param(struct VOEvent_Param *, const char*,
    const char*, const char*, const char*,
    const char*, const char*);
int add_param_to_group(
    xmlNodePtr, const char*, const char*,
    const char*, const char*, const char*,
    const char*, const char*, const char*);
int add_params_to_group(
    xmlNodePtr, const char*, const char*,
    const struct VOEvent_Param *, const unsigned int);
int add_param_to_what(
    xmlNodePtr, const char*, const char*,
    const char*, const char*, const char*,
    const char*);
int add_observatory_location(xmlNodePtr, const char*);
int add_astro_coords_2d(
    xmlNodePtr, const int, const struct tm*,
    const float, const struct VOEvent_Position2D *);
int add_astro_coords_3d(
    xmlNodePtr, const int, const struct tm*,
    const float, const struct VOEvent_Position3D *);
int fill_position_2d(
    struct VOEvent_Position2D*, const float, const char*, const float,
    const char*, const char*, const float);
int add_position_2d(xmlNodePtr, const struct VOEvent_Position2D*);
int fill_position_3d(
    struct VOEvent_Position3D*, const float, const char*, const float,
    const char*, const float, const char*, const char*);
int add_position_3d(xmlNodePtr, const struct VOEvent_Position3D*);
int add_time(xmlNodePtr, const struct tm*, const float);
int fill_reference(struct VOEvent_Reference*, const char*, const char*, const char*, const char*);
int add_reference(xmlNodePtr, const struct VOEvent_Reference*);
int fill_description(struct VOEvent_Description*, const char*);
int add_description(xmlNodePtr, const struct VOEvent_Description*);
int add_event_ivorn(xmlNodePtr, const char*, const int);
int fill_inference(
    struct VOEvent_Inference*, const float, const char*, const char*,
    const char*, const struct VOEvent_Description*,
    const struct VOEvent_Reference*);
int add_inference(xmlNodePtr, const struct VOEvent_Inference*);

// printing functions
void print_voevent_description(struct VOEvent_Description *);
void print_voevent_reference(struct VOEvent_Reference *);
void print_author(struct VOEvent *);
void print_voevent_header(struct VOEvent *);
void print_voevent_who(struct VOEvent *);
void print_event_ivorn(struct VOEvent_EventIVORN *);
void print_voevent_param(struct VOEvent_Param *);
void print_voevent_group(struct VOEvent_Group *);
void print_voevent_field(struct VOEvent_Field *);
void print_voevent_data(struct VOEvent_Data *, int, int);
void print_voevent_table(struct VOEvent_Table *);
void print_voevent_what(struct VOEvent *);
void print_voevent_position2d(struct VOEvent_Position2D *);
void print_voevent_position3d(struct VOEvent_Position3D *);
void print_voevent_timeinstant(struct VOEvent_TimeInstant *);
void print_voevent_time(struct VOEvent_Time *);
void print_voevent_astrocoords(struct VOEvent_AstroCoords *);
void print_voevent_observationlocation(struct VOEvent_ObservationLocation *);
void print_voevent_observatorylocation(struct VOEvent_ObservatoryLocation *);
void print_voevent_obsdatalocation(struct VOEvent_ObsDataLocation *);
void print_voevent_wherewhen(struct VOEvent *);
void print_voevent_how(struct VOEvent *);
void print_voevent_citations(struct VOEvent *);
void print_voevent_name(struct VOEvent_Name *);
void print_voevent_concept(struct VOEvent_Concept *);
void print_voevent_inference(struct VOEvent_Inference *);
void print_voevent_why(struct VOEvent *);
void print_voevent(struct VOEvent *);

// freeing functions
void free_voevent(struct VOEvent *);
void free_voevent_root(struct VOEvent *);
void free_voevent_author(struct VOEvent_Author *);
void free_voevent_reference(struct VOEvent_Reference *);
void free_voevent_description(struct VOEvent_Description *);
void free_voevent_param(struct VOEvent_Param *);
void free_voevent_group(struct VOEvent_Group *);
void free_voevent_who(struct VOEvent *);
void free_voevent_what(struct VOEvent *);
void free_voevent_wherewhen(struct VOEvent *);
void free_voevent_how(struct VOEvent *);
void free_voevent_citations(struct VOEvent *);
void free_voevent_why(struct VOEvent *);
void free_voevent_table(struct VOEvent_Table *);
void free_voevent_data(struct VOEvent_Data *);
void free_voevent_field(struct VOEvent_Field *);
void free_voevent_obsdatalocation(struct VOEvent_ObsDataLocation *);
void free_voevent_astrocoords(struct VOEvent_AstroCoords *);
void free_voevent_observationlocation(struct VOEvent_ObservationLocation *);
void free_voevent_observatorylocation(struct VOEvent_ObservatoryLocation *);
void free_voevent_time(struct VOEvent_Time *);
void free_voevent_position2d(struct VOEvent_Position2D *);
void free_voevent_position3d(struct VOEvent_Position3D *);
void free_voevent_timeinstant(struct VOEvent_TimeInstant *);
void free_voevent_event_ivorn(struct VOEvent_EventIVORN *);
void free_voevent_name(struct VOEvent_Name *);
void free_voevent_concept(struct VOEvent_Concept *);
void free_voevent_inference(struct VOEvent_Inference *);

// general purpose functions
char *get_ivorn(struct VOEvent *);
char *get_version(struct VOEvent *);
char *get_role(struct VOEvent *);
char *get_date(struct VOEvent *);
char *get_author_ivorn(struct VOEvent *);
struct VOEvent_Author *get_author(struct VOEvent *);
struct VOEvent_Param *get_param_by_name(struct VOEvent *, const char *);
struct VOEvent_Group *get_group_by_name(struct VOEvent *, const char *);
struct VOEvent_Param *get_param_by_index(struct VOEvent *, int);
struct VOEvent_Group *get_group_by_index(struct VOEvent *, int);
struct VOEvent_WhereWhen *get_wherewhen(struct VOEvent *);
struct VOEvent_ObsDataLocation *get_obsdata_location(struct VOEvent *);
struct VOEvent_ObservationLocation *get_observation_location(struct VOEvent *);
struct VOEvent_AstroCoords *get_astrocoord(struct VOEvent *);
struct VOEvent_Position2D *get_position2d(struct VOEvent *);
struct VOEvent_Position3D *get_position3d(struct VOEvent *);
struct VOEvent_Time *get_time(struct VOEvent *);
struct VOEvent_TimeInstant *get_time_instant(struct VOEvent *);
double get_c1(struct VOEvent *);
double get_c2(struct VOEvent *);
int get_trigger_time(struct VOEvent *, struct tm *);
int get_packet_time(struct VOEvent *, struct tm *);
int is_test(struct VOEvent *);
int is_observation(struct VOEvent *);
char *get_param_char_value(struct VOEvent *, const char *);
int get_param_int_value(struct VOEvent *, const char *);
int get_param_lint_value(struct VOEvent *, const char *);
int get_param_llint_value(struct VOEvent *, const char *);
double get_param_double_value(struct VOEvent *, const char *);
int get_ucd_description(const char *, char *);

// GCN specific (or more or less specific)
int get_packet_type(struct VOEvent *);
int get_packet_serial_number(struct VOEvent *);
int get_packet_sequence_number(struct VOEvent *);
double get_event_sod(struct VOEvent *);
int get_generic_event_time(struct VOEvent *, int *, int *, int *, int *, int *, double *);
double get_event_tjd(struct VOEvent *);
double get_packet_sod(struct VOEvent *);
double get_ra(struct VOEvent *);
double get_dec(struct VOEvent *);
double get_coord_error(struct VOEvent *);
long long int get_trigger_number(struct VOEvent *);
int get_event_counts(struct VOEvent *);
double get_event_significance(struct VOEvent *);
double get_trigger_significance(struct VOEvent *);
double get_data_significance(struct VOEvent *);
double get_phi(struct VOEvent *);
double get_theta(struct VOEvent *);
double get_spacecraft_longitude(struct VOEvent *);
double get_spacecraft_latitude(struct VOEvent *);
double get_trigger_timescale(struct VOEvent *);
double get_trigger_duration(struct VOEvent *);
double get_data_integration(struct VOEvent *);
double get_data_timescale(struct VOEvent *);
double get_integration_time(struct VOEvent *);
int get_algorithm_number(struct VOEvent *);
int get_most_likely_index(struct VOEvent *);
int get_most_likely_probability(struct VOEvent *);
int get_second_most_likely_index(struct VOEvent *);
int get_second_most_likely_probability(struct VOEvent *);
double get_hardness_ratio(struct VOEvent *);
long int get_trigger_id(struct VOEvent *);
int get_trigger_index(struct VOEvent *);
long int get_misc(struct VOEvent *);
long int get_dets(struct VOEvent *);
char *get_lightcurve_url(struct VOEvent *);
char *get_locationmap_url(struct VOEvent *);
int get_low_channel_index(struct VOEvent *);
int get_high_channel_index(struct VOEvent *);
int get_low_channel_energy(struct VOEvent *);
int get_high_channel_energy(struct VOEvent *);
double get_low_energy_range(struct VOEvent *);
double get_high_energy_range(struct VOEvent *);
int get_counts_channel(struct VOEvent *, int);
double get_source_temporal_ts(struct VOEvent *);
double get_source_image_ts(struct VOEvent *);

#endif
