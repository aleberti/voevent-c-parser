/**
 * @file voevent_utils.c
 * @brief Utility functions for VOEvents.
 *
 * Here are declared all the global variables, constants and the
 * new structures defined.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "voevent.h"

const char* roles[4] = {"observation", "prediction", "utility", "test"};
const char* astro_coord_systems[18] = {
    "TT-ICRS-TOPO"  , "UTC-ICRS-TOPO" , "TT-FK5-TOPO"  , "UTC-FK5-TOPO" ,
    "GPS-ICRS-TOPO" , "GPS-FK5-TOPO"  , "TT-ICRS-GEO"  , "UTC-ICRS-GEO" ,
    "TT-FK5-GEO"    , "UTC-FK5-GEO"   , "GPS-ICRS-GEO" , "GPS-FK5-GEO"  ,
    "TDB-ICRS-BARY" , "TDB-FK5-BARY"  , "UTC-HPC-TOPO" , "UTC-HPR-TOPO" ,
    "UTC-HGS-TOPO"  , "UTC-HGC-TOPO"
};
const char* observatory_locations[5] = {
    "GEOSURFACE", "GEOLEO", "GEOGSO", "GEONBH", "GEOLUN"
};
const char* cites[3] = {"followup", "supersedes", "retraction"};

const short int kNumUCDs = 547; ///< Number of UCD words defined by IVOA

const short int start_arith    = 0;
const short int start_em       = 10;
const short int start_em_ir    = 11;
const short int start_em_uv    = 24;
const short int start_em_x     = 29;
const short int start_em_gamma = 38;
const short int start_em_line  = 41;
const short int start_em_mm    = 51;
const short int start_em_opt   = 59;
const short int start_em_radio = 66;
const short int start_instr    = 81;
const short int start_meta     = 115;
const short int start_obs      = 166;
const short int start_phot     = 184;
const short int start_phys     = 205;
const short int start_pos      = 354;
const short int start_spect    = 439;
const short int start_src      = 458;
const short int start_stat     = 493;
const short int start_time     = 526;

const struct ucdlist ucd_ivoa[] = {
    {"arith",                              "Arithmetic quantities"},
    {"arith.diff",                         "Difference between two quantities described by the same UCD"},
    {"arith.factor",                       "Numerical factor"},
    {"arith.grad",                         "Gradient"},
    {"arith.rate",                         "Rate (per time unit)"},
    {"arith.ratio",                        "Ratio between two quantities described by the same UCD"},
    {"arith.squared",                      "Squared quantity"},
    {"arith.sum",                          "Summed or integrated quantity"},
    {"arith.variation",                    "Generic variation of a quantity"},
    {"arith.zp",                           "Zero point"},
    {"em",                                 "Electromagnetic spectrum"},
    {"em.IR",                              "Infrared part of the spectrum"},
    {"em.IR.J",                            "Infrared between 1.0 and 1.5 micron"},
    {"em.IR.H",                            "Infrared between 1.5 and 2 micron"},
    {"em.IR.K",                            "Infrared between 2 and 3 micron"},
    {"em.IR.3-4um",                        "Infrared between 3 and 4 micron"},
    {"em.IR.4-8um",                        "Infrared between 4 and 8 micron"},
    {"em.IR.8-15um",                       "Infrared between 8 and 15 micron"},
    {"em.IR.15-30um",                      "Infrared between 15 and 30 micron"},
    {"em.IR.30-60um",                      "Infrared between 30 and 60 micron"},
    {"em.IR.60-100um",                     "Infrared between 60 and 100 micron"},
    {"em.IR.NIR",                          "Near-Infrared, 1-5 microns"},
    {"em.IR.MIR",                          "Medium-Infrared, 5-30 microns"},
    {"em.IR.FIR",                          "Far-Infrared, 30-100 microns"},
    {"em.UV",                              "Ultraviolet part of the spectrum"},
    {"em.UV.10-50nm",                      "Ultraviolet between 10 and 50 nm EUV extreme UV"},
    {"em.UV.50-100nm",                     "Ultraviolet between 50 and 100 nm"},
    {"em.UV.100-200nm",                    "Ultraviolet between 100 and 200 nm FUV Far UV"},
    {"em.UV.200-300nm",                    "Ultraviolet between 200 and 300 nm NUV near UV"},
    {"em.X-ray",                           "X-ray part of the spectrum"},
    {"em.X-ray.soft",                      "Soft X-ray (0.12 - 2 keV)"},
    {"em.X-ray.medium",                    "Medium X-ray (2 - 12 keV)"},
    {"em.X-ray.hard",                      "Hard X-ray (12 - 120 keV)"},
    {"em.bin",                             "Channel / instrumental spectral bin coordinate (bin number)"},
    {"em.energy",                          "Energy value in the em frame"},
    {"em.freq",                            "Frequency value in the em frame"},
    {"em.freq.cutoff",                     "cutoff frequency"},
    {"em.freq.resonance",                  "resonance frequency"},
    {"em.gamma",                           "Gamma rays part of the spectrum"},
    {"em.gamma.soft",                      "Soft gamma ray (120 - 500 keV)"},
    {"em.gamma.hard",                      "Hard gamma ray (>500 keV)"},
    {"em.line",                            "Designation of major atomic lines"},
    {"em.line.HI",                         "21cm hydrogen line"},
    {"em.line.Lyalpha",                    "H-Lyalpha line"},
    {"em.line.Halpha",                     "H-alpha line"},
    {"em.line.Hbeta",                      "H-beta line"},
    {"em.line.Hgamma",                     "H-gamma line"},
    {"em.line.Hdelta",                     "H-delta line"},
    {"em.line.Brgamma",                    "Bracket gamma line"},
    {"em.line.OIII",                       "[OIII] line whose rest wl is 500.7 nm"},
    {"em.line.CO",                         "CO radio line, e.g 12CO(1-0) at 115GHz"},
    {"em.mm",                              "Millimetric/submillimetric part of the spectrum"},
    {"em.mm.30-50GHz",                     "Millimetric between 30 and 50 GHz"},
    {"em.mm.50-100GHz",                    "Millimetric between 50 and 100 GHz"},
    {"em.mm.100-200GHz",                   "Millimetric between 100 and 200 GHz"},
    {"em.mm.200-400GHz",                   "Millimetric between 200 and 400 GHz"},
    {"em.mm.400-750GHz",                   "Millimetric between 400 and 750 GHz"},
    {"em.mm.750-1500GHz",                  "Millimetric between 750 and 1500 GHz"},
    {"em.mm.1500-3000GHz",                 "Millimetric between 1500 and 3000 GHz"},
    {"em.opt",                             "Optical part of the spectrum"},
    {"em.opt.U",                           "Optical band between 300 and 400 nm"},
    {"em.opt.B",                           "Optical band between 400 and 500 nm"},
    {"em.opt.V",                           "Optical band between 500 and 600 nm"},
    {"em.opt.R",                           "Optical band between 600 and 750 nm"},
    {"em.opt.I",                           "Optical band between 750 and 1000 nm"},
    {"em.pw",                              "Plasma waves (trapped in local medium)"},
    {"em.radio",                           "Radio part of the spectrum"},
    {"em.radio.20MHz",                     "Radio below 20 MHz"},
    {"em.radio.20-100MHz",                 "Radio between 20 and 100 MHz"},
    {"em.radio.100-200MHz",                "Radio between 100 and 200 MHz"},
    {"em.radio.200-400MHz",                "Radio between 200 and 400 MHz"},
    {"em.radio.400-750MHz",                "Radio between 400 and 750 MHz"},
    {"em.radio.750-1500MHz",               "Radio between 750 and 1500 MHz"},
    {"em.radio.1500-3000MHz",              "Radio between 1500 and 3000 MHz"},
    {"em.radio.3-6GHz",                    "Radio between 3 and 6 GHz"},
    {"em.radio.6-12GHz",                   "Radio between 6 and 12 GHz"},
    {"em.radio.12-30GHz",                  "Radio between 12 and 30 GHz"},
    {"em.wavenumber",                      "Wavenumber value in the em frame"},
    {"em.wl",                              "Wavelength value in the em frame"},
    {"em.wl.central",                      "Central wavelength"},
    {"em.wl.effective",                    "Effective wavelength"},
    {"instr",                              "Instrument"},
    {"instr.background",                   "Instrumental background"},
    {"instr.bandpass",                     "Bandpass (e.g.: band name) of instrument"},
    {"instr.bandwidth",                    "Bandwidth of the instrument"},
    {"instr.baseline",                     "Baseline for interferometry"},
    {"instr.beam",                         "Beam"},
    {"instr.calib",                        "Calibration parameter"},
    {"instr.det",                          "Detector"},
    {"instr.det.noise",                    "Instrument noise"},
    {"instr.det.psf",                      "Point Spread Function"},
    {"instr.det.qe",                       "Quantum efficiency"},
    {"instr.dispersion",                   "Dispersion of a spectrograph"},
    {"instr.experiment",                   "Experiment or group of instruments"},
    {"instr.filter",                       "Filter"},
    {"instr.fov",                          "Field of view"},
    {"instr.obsty",                        "Observatory, satellite, mission"},
    {"instr.obsty.seeing",                 "Seeing"},
    {"instr.offset",                       "Offset angle respect to main direction of observation"},
    {"instr.order",                        "Spectral order in a spectrograph"},
    {"instr.param",                        "Various instrumental parameters"},
    {"instr.pixel",                        "Pixel (default size: angular)"},
    {"instr.plate",                        "Photographic plate"},
    {"instr.plate.emulsion",               "Plate emulsion"},
    {"instr.precision",                    "Instrument precision"},
    {"instr.rmsf",                         "Rotation Measure Spread Function"},
    {"instr.saturation",                   "Instrument saturation threshold"},
    {"instr.scale",                        "Instrument scale (for CCD, plate, image)"},
    {"instr.sensitivity",                  "Instrument sensitivity, detection threshold"},
    {"instr.setup",                        "Instrument configuration or setup"},
    {"instr.skyLevel",                     "Sky level"},
    {"instr.skyTemp",                      "Sky temperature"},
    {"instr.tel",                          "Telescope"},
    {"instr.tel.focalLength",              "Telescope focal length"},
    {"instr.voxel",                        "Related to a voxel (n-D volume element with n>2)"},
    {"meta",                               "Metadata"},
    {"meta.abstract",                      "Abstract (of paper, proposal, etc.)"},
    {"meta.bib",                           "Bibliographic reference"},
    {"meta.bib.author",                    "Author name"},
    {"meta.bib.bibcode",                   "Bibcode"},
    {"meta.bib.fig",                       "Figure in a paper"},
    {"meta.bib.journal",                   "Journal name"},
    {"meta.bib.page",                      "Page number"},
    {"meta.bib.volume",                    "Volume number"},
    {"meta.calibLevel",                    "Processing/calibration level"},
    {"meta.checksum",                      "Numerical signature of digital data"},
    {"meta.code",                          "Code or flag"},
    {"meta.code.class",                    "Classification code"},
    {"meta.code.error",                    "Limit uncertainty error flag"},
    {"meta.code.member",                   "Membership code"},
    {"meta.code.mime",                     "MIME type"},
    {"meta.code.multip",                   "Multiplicity or binarity flag"},
    {"meta.code.qual",                     "Quality, precision, reliability flag or code"},
    {"meta.code.status",                   "Status code (e.g.: status of a proposal/observation)"},
    {"meta.cryptic",                       "Unknown or impossible to understand quantity"},
    {"meta.curation",                      "Identity of man/organization responsible for the data"},
    {"meta.dataset",                       "Dataset"},
    {"meta.email",                         "Curation/contact e-mail"},
    {"meta.file",                          "File"},
    {"meta.fits",                          "FITS standard"},
    {"meta.id",                            "Identifier, name or designation"},
    {"meta.id.assoc",                      "Identifier of associated counterpart"},
    {"meta.id.CoI",                        "Name of Co-Investigator"},
    {"meta.id.cross",                      "Cross identification"},
    {"meta.id.parent",                     "Identification of parent source"},
    {"meta.id.part",                       "Part of identifier, suffix or sub-component"},
    {"meta.id.PI",                         "Name of Principal Investigator or Co-PI"},
    {"meta.main",                          "Main value of something"},
    {"meta.modelled",                      "Quantity was produced by a model"},
    {"meta.note",                          "Note or remark (longer than a code or flag)"},
    {"meta.number",                        "Number (of things; e.g. nb of object in an image)"},
    {"meta.preview",                       "Related to a preview operation for a dataset"},
    {"meta.query",                         "A query posed to an information system or database or a property of it"},
    {"meta.record",                        "Record number"},
    {"meta.ref",                           "Reference or origin"},
    {"meta.ref.doi",                       "DOI identifier (dereferenceable)"},
    {"meta.ref.ivoid",                     "Identifier as recommended in the IVOA (dereferenceable)"},
    {"meta.ref.ivorn",                     "Identifier defined as IVORN, VO Resource Name (ivo://) (deprecated)"},
    {"meta.ref.uri",                       "URI, universal resource identifier"},
    {"meta.ref.url",                       "URL, web address"},
    {"meta.software",                      "Software used in generating data"},
    {"meta.table",                         "Table or catalogue"},
    {"meta.title",                         "Title or explanation"},
    {"meta.ucd",                           "UCD"},
    {"meta.unit",                          "Unit"},
    {"meta.version",                       "Version"},
    {"obs",                                "Observation"},
    {"obs.airMass",                        "Airmass"},
    {"obs.atmos",                          "Atmosphere, atmospheric phenomena affecting an observation"},
    {"obs.atmos.extinction",               "Atmospheric extinction"},
    {"obs.atmos.refractAngle",             "Atmospheric refraction angle"},
    {"obs.calib",                          "Calibration observation"},
    {"obs.calib.flat",                     "Related to flat-field calibration observation (dome, sky, ..)"},
    {"obs.calib.dark",                     "Related to dark current calibration"},
    {"obs.exposure",                       "Exposure"},
    {"obs.field",                          "Region covered by the observation"},
    {"obs.image",                          "Image"},
    {"obs.observer",                       "Observer, discoverer"},
    {"obs.occult",                         "Observation of occultation phenomenon by solar system objects"},
    {"obs.transit",                        "Observation of transit phenomenon : exo-planets"},
    {"obs.param",                          "Various observation or reduction parameter"},
    {"obs.proposal",                       "Observation proposal"},
    {"obs.proposal.cycle",                 "Proposal cycle"},
    {"obs.sequence",                       "Sequence of observations, exposures or events"},
    {"phot",                               "Photometry"},
    {"phot.antennaTemp",                   "Antenna temperature"},
    {"phot.calib",                         "Photometric calibration"},
    {"phot.color",                         "Color index or magnitude difference"},
    {"phot.color.excess",                  "Color excess"},
    {"phot.color.reddFree",                "Dereddened color"},
    {"phot.count",                         "Flux expressed in counts"},
    {"phot.fluence",                       "Radiant photon energy received by a surface per unit area or irradiance of a surface integrated over time of irradiation"},
    {"phot.flux",                          "Photon flux or irradiance"},
    {"phot.flux.bol",                      "Bolometric flux"},
    {"phot.flux.density",                  "Flux density (per wl/freq/energy interval)"},
    {"phot.flux.density.sb",               "Flux density surface brightness"},
    {"phot.flux.sb",                       "Flux surface brightness"},
    {"phot.limbDark",                      "Limb-darkening coefficients"},
    {"phot.mag",                           "Photometric magnitude"},
    {"phot.mag.bc",                        "Bolometric correction"},
    {"phot.mag.bol",                       "Bolometric magnitude"},
    {"phot.mag.distMod",                   "Distance modulus"},
    {"phot.mag.reddFree",                  "Dereddened magnitude"},
    {"phot.mag.sb",                        "Surface brightness in magnitude units"},
    {"phot.radiance",                      "Radiance as energy flux per solid angle"},
    {"phys",                               "Physical quantities"},
    {"phys.SFR",                           "Star formation rate"},
    {"phys.absorption",                    "Extinction or absorption along the line of sight"},
    {"phys.absorption.coeff",              "Absorption coefficient (e.g. in a spectral line)"},
    {"phys.absorption.gal",                "Galactic extinction"},
    {"phys.absorption.opticalDepth",       "Optical depth"},
    {"phys.abund",                         "Abundance"},
    {"phys.abund.Fe",                      "Fe/H abundance"},
    {"phys.abund.X",                       "Hydrogen abundance"},
    {"phys.abund.Y",                       "Helium abundance"},
    {"phys.abund.Z",                       "Metallicity abundance"},
    {"phys.acceleration",                  "Acceleration"},
    {"phys.aerosol",                       "Relative to aerosol"},
    {"phys.albedo",                        "Albedo or reflectance"},
    {"phys.angArea",                       "Angular area"},
    {"phys.angMomentum",                   "Angular momentum"},
    {"phys.angSize",                       "Angular size width diameter dimension extension major minor axis extraction radius"},
    {"phys.angSize.smajAxis",              "Angular size extent or extension of semi-major axis"},
    {"phys.angSize.sminAxis",              "Angular size extent or extension of semi-minor axis"},
    {"phys.area",                          "Area (in surface, not angular units)"},
    {"phys.atmol",                         "Atomic and molecular physics (shared properties)"},
    {"phys.atmol.branchingRatio",          "Branching ratio"},
    {"phys.atmol.collisional",             "Related to collisions"},
    {"phys.atmol.collStrength",            "Collisional strength"},
    {"phys.atmol.configuration",           "Configuration"},
    {"phys.atmol.crossSection",            "Atomic / molecular cross-section"},
    {"phys.atmol.element",                 "Element"},
    {"phys.atmol.excitation",              "Atomic molecular excitation parameter"},
    {"phys.atmol.final",                   "Quantity refers to atomic/molecular final/ground state, level, etc."},
    {"phys.atmol.initial",                 "Quantity refers to atomic/molecular initial state, level, etc."},
    {"phys.atmol.ionStage",                "Ion, ionization stage"},
    {"phys.atmol.ionization",              "Related to ionization"},
    {"phys.atmol.lande",                   "Lande factor"},
    {"phys.atmol.level",                   "Atomic level"},
    {"phys.atmol.lifetime",                "Lifetime of a level"},
    {"phys.atmol.lineShift",               "Line shifting coefficient"},
    {"phys.atmol.number",                  "Atomic number Z"},
    {"phys.atmol.oscStrength",             "Oscillator strength"},
    {"phys.atmol.parity",                  "Parity"},
    {"phys.atmol.qn",                      "Quantum number"},
    {"phys.atmol.radiationType",           "Type of radiation characterizing atomic lines (electric dipole/quadrupole, magnetic dipole)"},
    {"phys.atmol.symmetry",                "Type of nuclear spin symmetry"},
    {"phys.atmol.sWeight",                 "Statistical weight"},
    {"phys.atmol.sWeight.nuclear",         "Statistical weight for nuclear spin states"},
    {"phys.atmol.term",                    "Atomic term"},
    {"phys.atmol.transition",              "Transition between states"},
    {"phys.atmol.transProb",               "Transition probability, Einstein A coefficient"},
    {"phys.atmol.wOscStrength",            "Weighted oscillator strength"},
    {"phys.atmol.weight",                  "Atomic weight"},
    {"phys.columnDensity",                 "Column density"},
    {"phys.composition",                   "Quantities related to composition of objects"},
    {"phys.composition.massLightRatio",    "Mass to light ratio"},
    {"phys.composition.yield",             "Mass yield"},
    {"phys.cosmology",                     "Related to cosmology"},
    {"phys.current",                       "Electric current"},
    {"phys.current.density",               "Electric current density"},
    {"phys.damping",                       "Generic damping quantities"},
    {"phys.density",                       "Density (of mass, electron, ...)"},
    {"phys.density.phaseSpace",            "Density in the phase space"},
    {"phys.dielectric",                    "Complex dielectric function"},
    {"phys.dispMeasure",                   "Dispersion measure"},
    {"phys.dust",                          "Relative to dust"},
    {"phys.electCharge",                   "Electric charge"},
    {"phys.electField",                    "Electric field"},
    {"phys.electron",                      "Electron"},
    {"phys.electron.degen",                "Electron degeneracy parameter"},
    {"phys.emissMeasure",                  "Emission measure"},
    {"phys.emissivity",                    "Emissivity"},
    {"phys.energy",                        "Energy"},
    {"phys.energy.Gibbs",                  "Gibbs (free) energy or free enthalpy [ G=H –TS ]"},
    {"phys.energy.Helmholtz",              "Helmholtz free energy [ A=U–TS ]"},
    {"phys.energy.density",                "Energy density"},
    {"phys.enthalpy",                      "Enthalpy [ H=U+pv ]"},
    {"phys.entropy",                       "Entropy"},
    {"phys.eos",                           "Equation of state"},
    {"phys.excitParam",                    "Excitation parameter U"},
    {"phys.fluence",                       "Particle energy received by a surface per unit area and integrated over time"},
    {"phys.flux",                          "Flux or flow of particle, energy, etc."},
    {"phys.flux.energy",                   "Energy flux, heat flux"},
    {"phys.gauntFactor",                   "Gaunt factor/correction"},
    {"phys.gravity",                       "Gravity"},
    {"phys.ionizParam",                    "Ionization parameter"},
    {"phys.ionizParam.coll",               "Collisional ionization"},
    {"phys.ionizParam.rad",                "Radiative ionization"},
    {"phys.luminosity",                    "Luminosity"},
    {"phys.luminosity.fun",                "Luminosity function"},
    {"phys.magAbs",                        "Absolute magnitude"},
    {"phys.magAbs.bol",                    "Bolometric absolute magnitude"},
    {"phys.magField",                      "Magnetic field"},
    {"phys.mass",                          "Mass"},
    {"phys.mass.inertiaMomentum",          "Momentum of inertia or rotational inertia"},
    {"phys.mass.loss",                     "Mass loss"},
    {"phys.mol",                           "Molecular data"},
    {"phys.mol.dipole",                    "Molecular dipole"},
    {"phys.mol.dipole.electric",           "Molecular electric dipole moment"},
    {"phys.mol.dipole.magnetic",           "Molecular magnetic dipole moment"},
    {"phys.mol.dissociation",              "Molecular dissociation"},
    {"phys.mol.formationHeat",             "Formation heat for molecules"},
    {"phys.mol.quadrupole",                "Molecular quadrupole"},
    {"phys.mol.quadrupole.electric",       "Molecular electric quadrupole moment"},
    {"phys.mol.rotation",                  "Molecular rotation"},
    {"phys.mol.vibration",                 "Molecular vibration"},
    {"phys.particle",                      "Related to physical particles"},
    {"phys.particle.neutrino",             "Related to neutrino"},
    {"phys.particle.neutron",              "Related to neutron"},
    {"phys.particle.proton",               "Related to proton"},
    {"phys.particle.alpha",                "Related to alpha particle"},
    {"phys.phaseSpace",                    "Related to phase space"},
    {"phys.polarization",                  "Polarization degree (or percentage)"},
    {"phys.polarization.circular",         "Circular polarization"},
    {"phys.polarization.coherency",        "Matrix of the correlation between components of an electromagnetic wave"},
    {"phys.polarization.linear",           "Linear polarization"},
    {"phys.polarization.rotMeasure",       "Rotation measure polarization"},
    {"phys.polarization.stokes",           "Stokes polarization"},
    {"phys.polarization.stokes.I",         "Stokes polarization coefficient I"},
    {"phys.polarization.stokes.Q",         "Stokes polarization coefficient Q"},
    {"phys.polarization.stokes.U",         "Stokes polarization coefficient U"},
    {"phys.polarization.stokes.V",         "Stokes polarization coefficient V"},
    {"phys.potential",                     "Potential (electric, gravitational, etc.)"},
    {"phys.pressure",                      "Pressure"},
    {"phys.recombination.coeff",           "Recombination coefficient"},
    {"phys.reflectance",                   "Radiance factor (received radiance divided by input radiance)"},
    {"phys.reflectance.bidirectional",     "Bidirectional reflectance"},
    {"phys.reflectance.bidirectional.df",  "Bidirectional reflectance distribution function"},
    {"phys.reflectance.factor",            "Reflectance normalized per direction cosine of incidence angle"},
    {"phys.refractIndex",                  "Refraction index"},
    {"phys.size",                          "Linear size, length (not angular)"},
    {"phys.size.axisRatio",                "Axis ratio (a/b) or (b/a)"},
    {"phys.size.diameter",                 "Diameter"},
    {"phys.size.radius",                   "Radius"},
    {"phys.size.smajAxis",                 "Linear semi major axis"},
    {"phys.size.sminAxis",                 "Linear semi minor axis"},
    {"phys.size.smedAxis",                 "Linear semi median axis for 3D ellipsoids"},
    {"phys.temperature",                   "Temperature"},
    {"phys.temperature.effective",         "Effective temperature"},
    {"phys.temperature.electron",          "Electron temperature"},
    {"phys.transmission",                  "Transmission (of filter, instrument, ...)"},
    {"phys.veloc",                         "Space velocity"},
    {"phys.veloc.ang",                     "Angular velocity"},
    {"phys.veloc.dispersion",              "Velocity dispersion"},
    {"phys.veloc.escape",                  "Escape velocity"},
    {"phys.veloc.expansion",               "Expansion velocity"},
    {"phys.veloc.microTurb",               "Microturbulence velocity"},
    {"phys.veloc.orbital",                 "Orbital velocity"},
    {"phys.veloc.pulsat",                  "Pulsational velocity"},
    {"phys.veloc.rotat",                   "Rotational velocity"},
    {"phys.veloc.transverse",              "Transverse / tangential velocity"},
    {"phys.virial",                        "Related to virial quantities (mass, radius, ..)"},
    {"phys.volume",                        "Volume (in cubic units)"},
    {"pos",                                "Position and coordinates"},
    {"pos.angDistance",                    "Angular distance, elongation"},
    {"pos.angResolution",                  "Angular resolution"},
    {"pos.az",                             "Position in alt-azimutal frame"},
    {"pos.az.alt",                         "Alt-azimutal altitude"},
    {"pos.az.azi",                         "Alt-azimutal azimut"},
    {"pos.az.zd",                          "Alt-azimutal zenith distance"},
    {"pos.azimuth",                        "Azimuthal angle in a generic reference plane"},
    {"pos.barycenter",                     "Barycenter"},
    {"pos.bodycentric",                    "Body-centric related coordinate"},
    {"pos.bodygraphic",                    "Body-graphic related coordinate"},
    {"pos.bodyrc",                         "Body related coordinates"},
    {"pos.bodyrc.alt",                     "Body related coordinate (altitude on the body)"},
    {"pos.bodyrc.lat",                     "Body related coordinate (latitude on the body)"},
    {"pos.bodyrc.lon",                     "Body related coordinate (longitude on the body)"},
    {"pos.cartesian",                      "Cartesian (rectangular) coordinates"},
    {"pos.cartesian.x",                    "Cartesian coordinate along the x-axis"},
    {"pos.cartesian.y",                    "Cartesian coordinate along the y-axis"},
    {"pos.cartesian.z",                    "Cartesian coordinate along the z-axis"},
    {"pos.centroid",                       "Related to centroid position"},
    {"pos.cmb",                            "Cosmic Microwave Background reference frame"},
    {"pos.cylindrical",                    "Related to cylindrical coordinates"},
    {"pos.cylindrical.azi",                "Azimuthal angle around z-axis (cylindrical coordinates)"},
    {"pos.cylindrical.r",                  "Radial distance from z-axis (cylindrical coordinates)"},
    {"pos.cylindrical.z",                  "Height or altitude from reference plane (cylindrical coordinates)"},
    {"pos.dirCos",                         "Direction cosine"},
    {"pos.distance",                       "Linear distance"},
    {"pos.earth",                          "Coordinates related to Earth"},
    {"pos.earth.altitude",                 "Altitude, height on Earth above sea level"},
    {"pos.earth.lat",                      "Latitude on Earth"},
    {"pos.earth.lon",                      "Longitude on Earth"},
    {"pos.ecliptic",                       "Ecliptic coordinates"},
    {"pos.ecliptic.lat",                   "Ecliptic latitude"},
    {"pos.ecliptic.lon",                   "Ecliptic longitude"},
    {"pos.emergenceAng",                   "Emergence angle of optical ray on an interface"},
    {"pos.eop",                            "Earth orientation parameters"},
    {"pos.ephem",                          "Ephemeris"},
    {"pos.eq",                             "Equatorial coordinates"},
    {"pos.eq.dec",                         "Declination in equatorial coordinates"},
    {"pos.eq.ha",                          "Hour-angle"},
    {"pos.eq.ra",                          "Right ascension in equatorial coordinates"},
    {"pos.eq.spd",                         "South polar distance in equatorial coordinates"},
    {"pos.errorEllipse",                   "Positional error ellipse"},
    {"pos.frame",                          "Reference frame used for positions"},
    {"pos.galactic",                       "Galactic coordinates"},
    {"pos.galactic.lat",                   "Latitude in galactic coordinates"},
    {"pos.galactic.lon",                   "Longitude in galactic coordinates"},
    {"pos.galactocentric",                 "Galactocentric coordinate system"},
    {"pos.geocentric",                     "Geocentric coordinate system"},
    {"pos.healpix",                        "Hierarchical Equal Area IsoLatitude Pixelization"},
    {"pos.heliocentric",                   "Heliocentric position coordinate (solar system bodies)"},
    {"pos.HTM",                            "Hierarchical Triangular Mesh"},
    {"pos.incidenceAng",                   "Incidence angle of optical ray on an interface"},
    {"pos.lambert",                        "Lambert projection"},
    {"pos.lg",                             "Local Group reference frame"},
    {"pos.lsr",                            "Local Standard of Rest reference frame"},
    {"pos.lunar",                          "Lunar coordinates"},
    {"pos.lunar.occult",                   "Occultation by lunar limb"},
    {"pos.nutation",                       "Nutation (of a body)"},
    {"pos.outline",                        "Set of points outlining a region (contour)"},
    {"pos.parallax",                       "Parallax"},
    {"pos.parallax.dyn",                   "Dynamical parallax"},
    {"pos.parallax.phot",                  "Photometric parallaxes"},
    {"pos.parallax.spect",                 "Spectroscopic parallax"},
    {"pos.parallax.trig",                  "Trigonometric parallax"},
    {"pos.phaseAng",                       "Phase angle, e.g. elongation of earth from sun as seen from a third cel. object"},
    {"pos.pm",                             "Proper motion"},
    {"pos.posAng",                         "Position angle of a given vector"},
    {"pos.precess",                        "Precession (in equatorial coordinates)"},
    {"pos.resolution",                     "Spatial linear resolution (not angular)"},
    {"pos.spherical",                      "Related to spherical coordinates"},
    {"pos.spherical.azi",                  "Azimuthal angle (spherical coordinates)"},
    {"pos.spherical.colat",                "Polar or Colatitude angle (spherical coordinates)"},
    {"pos.spherical.r",                    "Radial distance or radius (spherical coordinates)"},
    {"pos.supergalactic",                  "Supergalactic coordinates"},
    {"pos.supergalactic.lat",              "Latitude in supergalactic coordinates"},
    {"pos.supergalactic.lon",              "Longitude in supergalactic coordinates"},
    {"pos.wcs",                            "WCS keywords"},
    {"pos.wcs.cdmatrix",                   "WCS CDMATRIX"},
    {"pos.wcs.crpix",                      "WCS CRPIX"},
    {"pos.wcs.crval",                      "WCS CRVAL"},
    {"pos.wcs.ctype",                      "WCS CTYPE"},
    {"pos.wcs.naxes",                      "WCS NAXES"},
    {"pos.wcs.naxis",                      "WCS NAXIS"},
    {"pos.wcs.scale",                      "WCS scale or scale of an image"},
    {"spect",                              "Spectroscopy"},
    {"spect.binSize",                      "Spectral bin size"},
    {"spect.continuum",                    "Continuum spectrum"},
    {"spect.dopplerParam",                 "Doppler parameter b"},
    {"spect.dopplerVeloc",                 "Radial velocity, derived from the shift of some spectral feature"},
    {"spect.dopplerVeloc.opt",             "Radial velocity derived from a wavelength shift using the optical convention"},
    {"spect.dopplerVeloc.radio",           "Radial velocity derived from a frequency shift using the radio convention"},
    {"spect.index",                        "Spectral index"},
    {"spect.line",                         "Spectral line"},
    {"spect.line.asymmetry",               "Line asymmetry"},
    {"spect.line.broad",                   "Spectral line broadening"},
    {"spect.line.broad.Stark",             "Stark line broadening coefficient"},
    {"spect.line.broad.Zeeman",            "Zeeman broadening"},
    {"spect.line.eqWidth",                 "Line equivalent width"},
    {"spect.line.intensity",               "Line intensity"},
    {"spect.line.profile",                 "Line profile"},
    {"spect.line.strength",                "Spectral line strength S"},
    {"spect.line.width",                   "Spectral line full width half maximum"},
    {"spect.resolution",                   "Spectral (or velocity) resolution"},
    {"src",                                "Observed source viewed on the sky"},
    {"src.calib",                          "Calibration source"},
    {"src.calib.guideStar",                "Guide star"},
    {"src.class",                          "Source classification (star, galaxy, cluster, comet, asteroid )"},
    {"src.class.color",                    "Color classification"},
    {"src.class.distance",                 "Distance class e.g. Abell"},
    {"src.class.luminosity",               "Luminosity class"},
    {"src.class.richness",                 "Richness class e.g. Abell"},
    {"src.class.starGalaxy",               "Star/galaxy discriminator, stellarity index"},
    {"src.class.struct",                   "Structure classification e.g. Bautz-Morgan"},
    {"src.density",                        "Density of sources"},
    {"src.ellipticity",                    "Source ellipticity"},
    {"src.impactParam",                    "Impact parameter"},
    {"src.morph",                          "Morphology structure"},
    {"src.morph.param",                    "Morphological parameter"},
    {"src.morph.scLength",                 "Scale length for a galactic component (disc or bulge)"},
    {"src.morph.type",                     "Hubble morphological type (galaxies)"},
    {"src.net",                            "Qualifier indicating that a quantity (e.g. flux) is background subtracted rather than total"},
    {"src.orbital",                        "Orbital parameters"},
    {"src.orbital.eccentricity",           "Orbit eccentricity"},
    {"src.orbital.inclination",            "Orbit inclination"},
    {"src.orbital.meanAnomaly",            "Orbit mean anomaly"},
    {"src.orbital.meanMotion",             "Mean motion"},
    {"src.orbital.node",                   "Ascending node"},
    {"src.orbital.periastron",             "Periastron"},
    {"src.orbital.Tisserand",              "Tisserand parameter (generic)"},
    {"src.orbital.TissJ",                  "Tisserand parameter with respect to Jupiter"},
    {"src.redshift",                       "Redshift"},
    {"src.redshift.phot",                  "Photometric redshift"},
    {"src.sample",                         "Sample"},
    {"src.spType",                         "Spectral type MK"},
    {"src.var",                            "Variability of source"},
    {"src.var.amplitude",                  "Amplitude of variation"},
    {"src.var.index",                      "Variability index"},
    {"src.var.pulse",                      "Pulse"},
    {"stat",                               "Statistical parameters"},
    {"stat.asymmetry",                     "Measure of asymmetry"},
    {"stat.correlation",                   "Correlation between two parameters"},
    {"stat.covariance",                    "Covariance between two parameters"},
    {"stat.error",                         "Statistical error"},
    {"stat.error.sys",                     "Systematic error"},
    {"stat.filling",                       "Filling factor (volume, time, ..)"},
    {"stat.fit",                           "Fit"},
    {"stat.fit.chi2",                      "Chi2"},
    {"stat.fit.dof",                       "Degrees of freedom"},
    {"stat.fit.goodness",                  "Goodness or significance of fit"},
    {"stat.fit.omc",                       "Observed minus computed"},
    {"stat.fit.param",                     "Parameter of fit"},
    {"stat.fit.residual",                  "Residual fit"},
    {"stat.Fourier",                       "Fourier coefficient"},
    {"stat.Fourier.amplitude",             "Amplitude of Fourier coefficient"},
    {"stat.fwhm",                          "Full width at half maximum"},
    {"stat.interval",                      "Generic interval between two limits (defined as a pair of values)"},
    {"stat.likelihood",                    "Likelihood"},
    {"stat.max",                           "Maximum or upper limit"},
    {"stat.mean",                          "Mean, average value"},
    {"stat.median",                        "Median value"},
    {"stat.min",                           "Minimum or lowest limit"},
    {"stat.param",                         "Parameter"},
    {"stat.probability",                   "Probability"},
    {"stat.rank",                          "Rank or order in list of sorted values"},
    {"stat.rms",                           "Root mean square as square root of sum of squared values or quadratic mean"},
    {"stat.snr",                           "Signal to noise ratio"},
    {"stat.stdev",                         "Standard deviation as the square root of the variance"},
    {"stat.uncalib",                       "Qualifier of a generic uncalibrated quantity"},
    {"stat.value",                         "Miscellaneous value"},
    {"stat.variance",                      "Variance"},
    {"stat.weight",                        "Statistical weight"},
    {"time",                               "Time, generic quantity in units of time or date"},
    {"time.age",                           "Age"},
    {"time.creation",                      "Creation time/date (of dataset, file, catalogue,...)"},
    {"time.crossing",                      "Crossing time"},
    {"time.duration",                      "Interval of time describing the duration of a generic event or phenomenon"},
    {"time.end",                           "End time/date of a generic event"},
    {"time.epoch",                         "Instant of time related to a generic event (epoch, date, Julian date, time stamp/tag,...)"},
    {"time.equinox",                       "Equinox"},
    {"time.interval",                      "Time interval, time-bin, time elapsed between two events, not the duration of an event"},
    {"time.lifetime",                      "Lifetime"},
    {"time.period",                        "Period, interval of time between the recurrence of phases in a periodic phenomenon"},
    {"time.period.revolution",             "Period of revolution of a body around a primary one (similar to year)"},
    {"time.period.rotation",               "Period of rotation of a body around its axis (similar to day)"},
    {"time.phase",                         "Phase, position within a period"},
    {"time.processing",                    "A time/date associated with the processing of data"},
    {"time.publiYear",                     "Publication year"},
    {"time.relax",                         "Relaxation time"},
    {"time.release",                       "The time/date data is available to the public"},
    {"time.resolution",                    "Time resolution"},
    {"time.scale",                         "Timescale"},
    {"time.start",                         "Start time/date of generic event"}
};    ///< Structure containing all UCD words defined by IVOA (see http://www.ivoa.net/documents/UCD1+/20200212/PEN-UCDlist-1.4-20200212.pdf)

/**
 * @brief      Gets the VOEvent IVORN
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent IVORN
 */
char *get_ivorn(struct VOEvent *voevent){
    return (char *)((voevent->VOEvent)->ivorn);
}

/**
 * @brief      Gets the VOEvent version
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent version
 */
char *get_version(struct VOEvent * voevent){
    return (char *)((voevent->VOEvent)->version);
}

/**
 * @brief      Gets the VOEvent role
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent role
 */
char *get_role(struct VOEvent * voevent){
    return (char *)((voevent->VOEvent)->role);
}

/**
 * @brief      Check if VOEvent is a test alert
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     1 if test alert, 0 otherwise
 */
int is_test(struct VOEvent * voevent){
    char *role = get_role(voevent);
    if(strcmp(role, "test") == 0){
        return 1;
    }

    return 0;
}

/**
 * @brief      Check if VOEvent is an observation alert
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     1 if observation alert, 0 otherwise
 */
int is_observation(struct VOEvent * voevent){
    char *role = get_role(voevent);
    if(strcmp(role, "observation") == 0){
        return 1;
    }

    return 0;
}

/**
 * @brief      Gets the VOEvent date
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent date
 */
char *get_date(struct VOEvent * voevent){
    return (char *)((voevent->Who)->Date);
}

/**
 * @brief      Gets the VOEvent AuthorIVORN
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent AuthorIVORN
 */
char *get_author_ivorn(struct VOEvent * voevent){
    return (char *)((voevent->Who)->AuthorIVORN);
}

/**
 * @brief      Gets the VOEvent Author element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     The VOEvent Author
 */
struct VOEvent_Author *get_author(struct VOEvent *voevent){
    return ((voevent->Who)->Author);
}

/**
 * @brief      Search for a Parameter element with the given name
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     A VOEvent_Param structure if found, NULL otherwise
 */
struct VOEvent_Param *get_param_by_name(struct VOEvent *voevent, const char *param_name){
    int i;
    for (i = 0; i < voevent->What->nr_params; ++i){
        if(voevent->What->Param[i].name){
            if (strcmp((char*)voevent->What->Param[i].name, param_name) == 0)
            {
                return &(voevent->What->Param[i]);
            }
        }
    }

    int j;
    for (j = 0; j < voevent->What->nr_groups; ++j){

        int w;
        for (w = 0; w < voevent->What->Group[j].nr_params; ++w)
        {
            if(voevent->What->Group[j].Param[w].name){
                if (strcmp((char*)voevent->What->Group[j].Param[w].name, param_name) == 0)
                {
                    return &(voevent->What->Group[j].Param[w]);
                }  
            }  
        }
    }

    return NULL;
}

/**
 * @brief      Search for a Parameter element with the given name and retrieve its char value
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     Char value of the parameter if found, NULL otherwise
 */
char *get_param_char_value(struct VOEvent *voevent, const char *param_name){
    struct VOEvent_Param *param = get_param_by_name(voevent, param_name);
    if(param){
        return ((char *)param->value);
    }
    else{
        return NULL;
    }
}

/**
 * @brief      Search for a Parameter element with the given name and retrieve its integer value
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     Integer value of the parameter if found, -1 otherwise
 */
int get_param_int_value(struct VOEvent *voevent, const char *param_name){
    struct VOEvent_Param *param = get_param_by_name(voevent, param_name);
    if(param){
        return atoi((char *)param->value);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Search for a Parameter element with the given name and retrieve its long integer value
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     Long integer value of the parameter if found, -1 otherwise
 */
int get_param_lint_value(struct VOEvent *voevent, const char *param_name){
    struct VOEvent_Param *param = get_param_by_name(voevent, param_name);
    if(param){
        return strtol((char *)param->value, NULL, 10);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Search for a Parameter element with the given name and retrieve its long long integer value
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     Long long integer value of the parameter if found, -1 otherwise
 */
int get_param_llint_value(struct VOEvent *voevent, const char *param_name){
    struct VOEvent_Param *param = get_param_by_name(voevent, param_name);
    if(param){
        return strtoll((char *)param->value, NULL, 10);
    }
    else{
        return -1;
    }
}

/**
 * @brief      Search for a Parameter element with the given name and retrieve its double value
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  param_name  The parameter name
 *
 * @return     Double value of the parameter if found, -1 otherwise
 */
double get_param_double_value(struct VOEvent *voevent, const char *param_name){
    struct VOEvent_Param *param = get_param_by_name(voevent, param_name);
    if(param){
        return atof((char *)param->value);
    }
    else{
        return -1.0;
    }
}

/**
 * @brief      Search for a Group element with the given name
 *
 * @param      voevent     A VOEvent structure
 * @param[in]  group_name  The group name
 *
 * @return     A VOEvent_Group structure if found, NULL otherwise
 */
struct VOEvent_Group *get_group_by_name(struct VOEvent *voevent, const char *group_name){
    int i;
    for (i = 0; i < voevent->What->nr_groups; ++i){
        
        if(voevent->What->Group[i].name){
            if(strcmp((char*)voevent->What->Group[i].name, group_name) == 0){
                return &(voevent->What->Group[i]);
            }
        }
    }

    return NULL; 
}

/**
 * @brief      Search for the Parameter element with given index (remember that arrays start from 0)
 *
 * @param      voevent  A VOEvent structure
 * @param[in]  index    The index
 *
 * @return     A VOEvent_Param structure if index is valid, NULL otherwise
 */
struct VOEvent_Param *get_param_by_index(struct VOEvent *voevent, int index){
    if (index > voevent->What->nr_params)
    {
        fprintf(stderr, "Index out of range. Maximum number of parameters in this VOEvent is %d.\n", voevent->What->nr_params);
        return NULL;
    }

    return &(voevent->What->Param[index-1]);

}

/**
 * @brief      Search for the Group element with given index (remember that arrays start from 0)
 *
 * @param      voevent  A VOEvent structure
 * @param[in]  index    The index
 *
 * @return     A VOEvent_Group structure if index is valid, NULL otherwise
 */
struct VOEvent_Group *get_group_by_index(struct VOEvent *voevent, int index){
    if (index > voevent->What->nr_groups)
    {
        fprintf(stderr, "Index out of range. Maximum number of groups in this VOEvent is %d.\n", voevent->What->nr_groups);
        return NULL;
    }

    return &(voevent->What->Group[index-1]);
}

/**
 * @brief      Get WhereWhen element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_WhereWhen structure if found, NULL otherwise
 */
struct VOEvent_WhereWhen *get_wherewhen(struct VOEvent *voevent){
    if(voevent->WhereWhen){
        return voevent->WhereWhen;
    }

    return NULL;
}

/**
 * @brief      Get ObsDataLocation element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_ObsDataLocation structure if found, NULL otherwise
 */
struct VOEvent_ObsDataLocation *get_obsdata_location(struct VOEvent *voevent){
    if(get_wherewhen(voevent)){
        struct VOEvent_WhereWhen *wherewhen = get_wherewhen(voevent);
        return wherewhen->ObsDataLocation;
    }

    return NULL;
}

/**
 * @brief      Get ObservationLocation element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_ObservationLocation structure if found, NULL otherwise
 */
struct VOEvent_ObservationLocation *get_observation_location(struct VOEvent *voevent){
    if(get_obsdata_location(voevent)){
        struct VOEvent_ObsDataLocation *obsdatalocation = get_obsdata_location(voevent);
        return obsdatalocation->ObservationLocation;
    }

    return NULL;
}

/**
 * @brief      Get AstroCoords element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_AstroCoords structure if found, NULL otherwise
 */
struct VOEvent_AstroCoords *get_astrocoord(struct VOEvent *voevent){
    if(get_observation_location(voevent)){
        struct VOEvent_ObservationLocation *observationlocation = get_observation_location(voevent);
        return observationlocation->AstroCoords;
    }

    return NULL;
}

/**
 * @brief      Get Position2D element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_Position2D structure if found, NULL otherwise
 */
struct VOEvent_Position2D *get_position2d(struct VOEvent *voevent){
    if (get_astrocoord(voevent))
    {
        struct VOEvent_AstroCoords *astrocoords = get_astrocoord(voevent);
        return astrocoords->Position2D;
    }

    return NULL;
}

/**
 * @brief      Get Position3D element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_Position3D structure if found, NULL otherwise
 */
struct VOEvent_Position3D *get_position3d(struct VOEvent *voevent){
    if (get_astrocoord(voevent))
    {
        struct VOEvent_AstroCoords *astrocoords = get_astrocoord(voevent);
        return astrocoords->Position3D;
    }

    return NULL;
}

/**
 * @brief      Get Time element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_Time structure if found, NULL otherwise
 */
struct VOEvent_Time *get_time(struct VOEvent *voevent){
    if (get_astrocoord(voevent))
    {
        struct VOEvent_AstroCoords *astrocoords = get_astrocoord(voevent);
        return astrocoords->Time;
    }

    return NULL;
}

/**
 * @brief      Get TimeInstant element
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     VOEvent_TimeInstant structure if found, NULL otherwise
 */
struct VOEvent_TimeInstant *get_time_instant(struct VOEvent *voevent){
    if (get_time(voevent))
    {
        struct VOEvent_Time *time = get_time(voevent);
        return time->TimeInstant;
    }

    return NULL;
}

/**
 * @brief      Get C1 value
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     C1 value if found, -1 otherwise
 */
double get_c1(struct VOEvent *voevent){

    struct VOEvent_Position2D *position2d = get_position2d(voevent);
    if(position2d){
        return atof((char *)position2d->C1);
    }

    return -1;
}

/**
 * @brief      Get C2 value
 *
 * @param      voevent  A VOEvent structure
 *
 * @return     C2 value if found, -1 otherwise
 */
double get_c2(struct VOEvent *voevent){

    struct VOEvent_Position2D *position2d = get_position2d(voevent);
    if(position2d){
        return atof((char *)position2d->C2);
    }

    return -1;
}

/**
 * @brief      Get trigger time of the event and put it in a tm structure
 *
 * @param      voevent       A VOEvent structure
 * @param      trigger_time  tm structure containing the info about the trigger time
 */
int get_trigger_time(struct VOEvent *voevent, struct tm *trigger_time){
    if (get_time_instant(voevent))
    {
        struct VOEvent_TimeInstant *time_instant = get_time_instant(voevent);
        //printf("%s\n", (char *)time_instant->ISOTime);
        char str[10];
        int n;
        n = sscanf((char *)time_instant->ISOTime, "%4d-%2d-%2d %1s %2d:%2d:%2d", &(trigger_time->tm_year), &(trigger_time->tm_mon), &(trigger_time->tm_mday), str, &(trigger_time->tm_hour), &(trigger_time->tm_min), &(trigger_time->tm_sec));
        if(n == 7){
            trigger_time->tm_year -= 1900;
            trigger_time->tm_mon -= 1;
            return 1;
        }
        else{
            fprintf(stderr, "Trigger time not scanned properly.\n");
            return 0;
        }
    }

    return 0;
}

/**
 * @brief      Get packet time and put it in a tm structure
 *
 * @param      voevent       A VOEvent structure
 * @param      packet_time   tm structure containing the info about the packet time
 */
int get_packet_time(struct VOEvent *voevent, struct tm *packet_time){
    if (get_date(voevent))
    {
        char *pkt_time = get_date(voevent);
        //printf("%s\n", pkt_time);
        char str[10];
        int n;
        n = sscanf(pkt_time, "%4d-%2d-%2d %1s %2d:%2d:%2d", &(packet_time->tm_year), &(packet_time->tm_mon), &(packet_time->tm_mday), str, &(packet_time->tm_hour), &(packet_time->tm_min), &(packet_time->tm_sec));
        if(n == 7){
            packet_time->tm_year -= 1900;
            packet_time->tm_mon -= 1;
            return 1;
        }
        else{
            fprintf(stderr, "Packet time not scanned properly.\n");
            return 0;
        }
    }

    return 0;
}

/**
 * @brief      Get event SOD from Transport message.
 *
 * @param      transport       A Transport structure
 *
 * @return     SOD value if found, 0 otherwise.
 */
double get_transport_sod(struct Transport *transport){

    int year, mon, day, hour, min;
    double sec;

    if (transport->TimeStamp)
    {
        char str[10];
        int n;
        n = sscanf((char *)transport->TimeStamp, "%4d-%2d-%2d %1s %2d:%2d:%lf", &year, &mon, &day, str, &hour, &min, &sec);
        if(n == 7){
            int sod = (double)hour * 3600.0 + (double)min * 60.0 + sec;
            return sod;
        }
        else{
            fprintf(stderr, "Packet time not scanned properly.\n");
            return 0;
        }
    }

    return 0;
}

/**
 * @brief      Get the description of the given UCD word.
 *
 * If a match with the given UCD is found, the string buffer is filled
 * with the corresponding UCD description, if the buffer is big enough,
 * and 0 is returned. If not, the string buffer will be set to NULL and
 * 1 is returned. If no match is found, 1 is returned.
 *
 * @param      ucd                       The UCD word
 * @param      ucd_description_buffer    String buffer where description is stored
 *
 * @return     0 if UCD match is found and string buffer is big enough, 1 otherwise.
 */
int get_ucd_description(const char *ucd, char *ucd_description_buffer){

    if(ucd == NULL || ucd_description_buffer == NULL){
        fprintf(stderr, "UCD string pointer or string buffer pointer invalid.\n");
        return 1;
    }

    int start_index = 0;

    if(strstr(ucd, "arith")){
        start_index = start_arith;
    }
    else if(strstr(ucd, "em")){
        start_index = start_em;
    }
    else if(strstr(ucd, "em.IR")){
        start_index = start_em_ir;
    }
    else if(strstr(ucd, "em.UV")){
        start_index = start_em_uv;
    }
    else if(strstr(ucd, "em.X")){
        start_index = start_em_x;
    }
    else if(strstr(ucd, "em.gamma")){
        start_index = start_em_gamma;
    }
    else if(strstr(ucd, "em.line")){
        start_index = start_em_line;
    }
    else if(strstr(ucd, "em.mm")){
        start_index = start_em_mm;
    }
    else if(strstr(ucd, "em.opt")){
        start_index = start_em_opt;
    }
    else if(strstr(ucd, "em.radio")){
        start_index = start_em_radio;
    }
    else if(strstr(ucd, "instr")){
        start_index = start_instr;
    }
    else if(strstr(ucd, "meta")){
        start_index = start_meta;
    }
    else if(strstr(ucd, "obs")){
        start_index = start_obs;
    }
    else if(strstr(ucd, "phot")){
        start_index = start_phot;
    }
    else if(strstr(ucd, "phys")){
        start_index = start_phys;
    }
    else if(strstr(ucd, "pos")){
        start_index = start_pos;
    }
    else if(strstr(ucd, "spect")){
        start_index = start_spect;
    }
    else if(strstr(ucd, "src")){
        start_index = start_src;
    }
    else if(strstr(ucd, "stat")){
        start_index = start_stat;
    }
    else if(strstr(ucd, "time")){
        start_index = start_time;
    }
    else{
        start_index = 0;
    }

    int i = 0;

    for (i = start_index; i < kNumUCDs; ++i){
        if(strcmp(ucd, ucd_ivoa[i].ucd_word) == 0){
            if(strlen(ucd_description_buffer) <= strlen(ucd_ivoa[i].ucd_description) + 1){
                memcpy(
                    ucd_description_buffer,
                    ucd_ivoa[i].ucd_description,
                    strlen(ucd_ivoa[i].ucd_description)+1);
                return 0;
            }
            else{
                fprintf(stderr, "String buffer size not big enough for the description of UCD word: %s\n", ucd);
                return 1;
            }
        }
    }

    return 1;
}
