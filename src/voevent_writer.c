/**
 * @file voevent_writer.c
 * @brief Source file containing functions to write VOEvents.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "voevent.h"

/**
 * @brief      Create XML document pointer
 *
 * @return     Document pointer if it was created correctly, NULL otherwise
 */
xmlDocPtr create_doc(void){

    xmlDocPtr doc = NULL;

    // Build document pointer and assign it to the Doc element of VOEvent structure
    doc = xmlNewDoc(BAD_CAST "1.0");
    if (doc == NULL){
        fprintf(stderr, "Error in creating new document pointer.\n");
        return NULL;
    }

    return doc;
}

/**
 * @brief      Create XML root (VOEvent) node
 *
 * The IVORN attribute is required by the VOEvent schema, so it cannot
 * be passed as NULL. The role atttibute is optional according to the
 * schema, but here the user is required to pass explicitly one value
 * of the role attribute from the ::roles_values enum. The version number
 * is mandatory, but it is hardcoded to be 2.0 inside the function, since
 * the library works with the VOEvent 2.0 standard.
 *
 * @param[in]  doc       XML document pointer (cannot be NULL)
 * @param[in]  ivorn     The VOEvent 'ivorn' attribute (cannot be NULL)
 * @param[in]  role      The VOEvent 'role' attribute (value from ::roles_values enum)
 *
 * @return     0 if root node was created correctly, 1 otherwise
 */
xmlNodePtr create_root_node(xmlDocPtr doc, const char *ivorn, const int role){

    if (doc == NULL){
        fprintf(stderr, "Error: document pointer cannot be NULL.\n");
        return NULL;
    }

    if (role < 0 || role > MAX_ROLES - 1){
        fprintf(stderr, "Error: role value not recognized.\n");
        return NULL;
    }

    if (ivorn == NULL){
        fprintf(stderr, "Error: IVORN cannot be NULL.\n");
        return NULL;
    }

    // Build root (VOEvent) node (with attributes) and assign it to the VOEvent_root structure
    xmlNodePtr root_node = NULL;
    root_node = xmlNewNode(NULL, BAD_CAST "voe:VOEvent");
    if (root_node == NULL){
        fprintf(stderr, "Error: cannot build root node.\n");
        return NULL;
    }
    xmlDocSetRootElement(doc, root_node);
    xmlNewProp(root_node, BAD_CAST "role", BAD_CAST roles[role]);
    xmlNewProp(root_node, BAD_CAST "version", BAD_CAST "2.0");
    xmlNewProp(root_node, BAD_CAST "ivorn", BAD_CAST ivorn);
    xmlNewProp(root_node, BAD_CAST "xmlns:voe", BAD_CAST "http://www.ivoa.net/xml/VOEvent/v2.0");
    xmlNewProp(root_node, BAD_CAST "xmlns:xsi", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance");
    xmlNewProp(root_node, BAD_CAST "xsi:schemaLocation", BAD_CAST "http://www.ivoa.net/xml/VOEvent/v2.0 http://www.ivoa.net/xml/VOEvent/VOEvent-v2.0.xsd");

    return root_node;
}

/**
 * @brief      Create VOEvent Who element
 *
 * If date is passed as NULL, then it is added in the format
 * YYYY-MM-DDTHH:MM:SS (UTC) as the current time. If author_ivorn
 * is NULL, nothing is added.
 *
 * @param[in]  root_node     Root node (cannot be NULL)
 * @param[in]  date          Date (can be NULL)
 * @param[in]  author_ivorn  Author IVORN (can be NULL)
 * @param[in]  description   A Description element (can be NULL)
 * @param[in]  reference     A reference element (can be NULL)
 *
 * @return     Who element node pointer
 */
xmlNodePtr create_who(
    xmlNodePtr root_node, const struct tm *date, const char* author_ivorn,
    const struct VOEvent_Description* description, const struct VOEvent_Reference* reference){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    int status;

    xmlNodePtr who_node = xmlNewChild(root_node, NULL, BAD_CAST "Who", NULL);
    if (date){
        char datebuf[21];
        strftime(datebuf,21,"%FT%T", date);
        xmlNewChild(who_node, NULL, BAD_CAST "Date", BAD_CAST datebuf);
    }
    else{
        char datebuf[21];
        struct tm *t;
        time_t t_now;

        time(&t_now);
        t = gmtime(&t_now);
        strftime(datebuf,21,"%FT%T",t);
        xmlNewChild(who_node, NULL, BAD_CAST "Date", BAD_CAST datebuf);
    }
    if (author_ivorn){
        xmlNewChild(who_node, NULL, BAD_CAST "AuthorIVORN", BAD_CAST author_ivorn);
    }
    if (description){
        status = add_description(who_node, description);
        if (status){
            fprintf(stderr, "Error: could not add Description to Who element.\n");
            return NULL;
        }
    }
    if (reference){
        status = add_reference(who_node, reference);
        if (status){
            fprintf(stderr, "Error: could not add Reference to Who element.\n");
            return NULL;
        }
    }
    return who_node;
}

/**
 * @brief      Create VOEvent What element
 *
 * @param[in]  root_node     Root node (cannot be NULL)
 * @param[in]  description   A Description element (can be NULL)
 * @param[in]  reference     A reference element (can be NULL)
 *
 * @return     What element node pointer
 */
xmlNodePtr create_what(
    xmlNodePtr root_node, const struct VOEvent_Description* description,
    const struct VOEvent_Reference* reference){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    int status;

    xmlNodePtr what_node = xmlNewChild(root_node, NULL, BAD_CAST "What", NULL);

    if (description){
        status = add_description(what_node, description);
        if (status){
            fprintf(stderr, "Error: could not add Description to Who element.\n");
            return NULL;
        }
    }
    if (reference){
        status = add_reference(what_node, reference);
        if (status){
            fprintf(stderr, "Error: could not add Reference to Who element.\n");
            return NULL;
        }
    }

    return what_node;
}

/**
 * @brief      Create VOEvent WhereWhen element
 *
 * @param[in]  root_node     Root node (cannot be NULL)
 * @param[in]  id            WhereWhen element ID (can be NULL)
 *
 * @return     ObsDataLocation element node pointer
 */
xmlNodePtr create_wherewhen(xmlNodePtr root_node, const char *id){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    xmlNodePtr wherewhen_node = xmlNewChild(root_node, NULL, BAD_CAST "WhereWhen", NULL);

    if (wherewhen_node == NULL){
        fprintf(stderr, "Error in building Wherewhen element.\n");
        return NULL;
    }

    if (id){
        xmlNewProp(wherewhen_node, BAD_CAST "id", BAD_CAST id);
    }
    xmlNodePtr obsdata_node = xmlNewChild(wherewhen_node, NULL, BAD_CAST "ObsDataLocation", NULL);

    if (obsdata_node == NULL){
        fprintf(stderr, "Error in building ObsDataLocation element in WhereWhen element.\n");
        return NULL;
    }

    return obsdata_node;
}

/**
 * @brief      Create How element node.
 *
 * At least one between the reference and description arguments
 * should be not NULL.
 *
 * @param[in]  root_node      XML root node (VOEvent) (cannot be NULL)
 * @param[in]  reference      VOEvent_Reference structure
 * @param[in]  description    VOEvent_Description structure
 *
 * @return     0 if root node was created correctly, 1 otherwise
 */
xmlNodePtr create_how(
    xmlNodePtr root_node, const struct VOEvent_Reference* reference,
    const struct VOEvent_Description* description){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    if (reference == NULL && description == NULL){
        fprintf(stderr, "Error: you should pass at least one not-NULL argument.\n");
        return NULL;
    }

    xmlNodePtr how_node = xmlNewChild(root_node, NULL, BAD_CAST "How", NULL);
    if (how_node == NULL){
        fprintf(stderr, "Error: could not build How element.\n");
        return NULL;
    }

    int status;

    if (reference){
        status = add_reference(how_node, reference);
        if (status){
            fprintf(stderr, "Error: could not add Reference element.\n");
            return NULL;
        }
    }

    if (description){
        status = add_description(how_node, description);
        if (status){
            fprintf(stderr, "Error: could not add Sescription element.\n");
            return NULL;
        }
    }

    return how_node;
}

/**
 * @brief      Create Why element node.
 *
 * At least one between the reference and description arguments
 * should be not NULL.
 *
 * @param[in]  root_node   XML root node (VOEvent) (cannot be NULL)
 * @param[in]  importance  Importance attribute
 * @param[in]  expires     Expires attribute (a struct tm) (can be NULL)
 * @param[in]  inference   VOEvent_Inference structure (can be NULL)
 *
 * @return     0 if root node was created correctly, 1 otherwise
 */
xmlNodePtr create_why(
    xmlNodePtr root_node, const float importance, const struct tm *expires,
    const struct VOEvent_Inference* inference){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    xmlNodePtr why_node = xmlNewChild(root_node, NULL, BAD_CAST "Why", NULL);
    if (why_node == NULL){
        fprintf(stderr, "Error: could not build How element.\n");
        return NULL;
    }

    char buffer[24];
    sprintf(buffer, "%.3f", importance);
    xmlNewProp(why_node, BAD_CAST "importance", BAD_CAST buffer);

    if (expires){
        char datebuf[21];
        strftime(datebuf,21,"%FT%T",expires);
        xmlNewProp(why_node, BAD_CAST "expires", BAD_CAST datebuf);;
    }

    int status;

    if (inference){
        status = add_inference(why_node, inference);
        if (status){
            fprintf(stderr, "Error: could not add Inference element.\n");
            return NULL;
        }
    }

    return why_node;
}

/**
 * @brief      Create Citations element node.
 *
 * When creating the Citations element, the creation of one
 * EventIVORN element is enforced. Other EventIVORN elements
 * can be added with the add_event_ivorn() function.
 *
 * @param[in]  root_node      XML root node (VOEvent) (cannot be NULL)
 * @param[in]  event_ivorn    Event IVORN (cannot be NULL)
 * @param[in]  cite           Cite attribute of Event IVORN (value from ::cites_values enum)
 * @param[in]  description    VOEvent_Description structure (can be NULL)
 *
 * @return     0 if root node was created correctly, 1 otherwise
 */
xmlNodePtr create_citations(
    xmlNodePtr root_node, const char* event_ivorn, const int cite,
    const struct VOEvent_Description* description){

    if (root_node == NULL){
        fprintf(stderr, "Error: root node cannot be NULL.\n");
        return NULL;
    }

    if (event_ivorn == NULL){
        fprintf(stderr, "Error: EventIVORN cannot be NULL.\n");
        return NULL;
    }

    int status;

    xmlNodePtr citations_node = xmlNewChild(root_node, NULL, BAD_CAST "Citations", NULL);
    if (citations_node == NULL){
        fprintf(stderr, "Error: could not build Citations element.\n");
        return NULL;
    }

    status = add_event_ivorn(citations_node, event_ivorn, cite);
    if (status){
        fprintf(stderr, "Error: could not add EventIVORN element.\n");
        return NULL;
    }

    if (description){
        status = add_description(citations_node, description);
        if (status){
            fprintf(stderr, "Error: could not add Description element.\n");
            return NULL;
        }
    }

    return citations_node;
}

/**
 * @brief      Adds Author element to Who element.
 *
 * At least the short name of the Author should be passed as not NULL string.
 *
 * @param[in]  who_node         Who element node (cannot be NULL)
 * @param[in]  short_name       Author short name (cannot be NULL)
 * @param[in]  title            Author title (can be NULL)
 * @param[in]  logo_url         Author logo URL (can be NULL)
 * @param[in]  contact_name     Author contact name (can be NULL)
 * @param[in]  contact_email    Author contact email (can be NULL)
 * @param[in]  contact_phone    Author contact phone number (can be NULL)
 * @param[in]  contributor      Author contributor (can be NULL)
 *
 * @return     0 if no error occurred, 1 otherwise
 */
int add_author(
    xmlNodePtr who_node, const char* short_name, const char* title,
    const char* logo_url, const char* contact_name, const char* contact_email,
    const char* contact_phone, const char* contributor){

    if (who_node == NULL){
        fprintf(stderr, "Error: Who element node cannot be NULL.\n");
        return 1;
    }

    if (xmlStrcmp(who_node->name, (const xmlChar *)"Who")){
        fprintf(stderr, "Error: trying to add Author element outside Who element.\n");
        return 1;
    }

    if (short_name == NULL){
        fprintf(stderr, "Error: specify short name of the Author.\n");
        return 1;
    }

    xmlNodePtr author_node = NULL;
    author_node = xmlNewChild(who_node, NULL, BAD_CAST "Author", NULL);
    if (author_node == NULL){
        fprintf(stderr, "Error: could not build Author element.\n");
        return 1;
    }

    xmlNewChild(author_node, NULL, BAD_CAST "shortName", BAD_CAST short_name);

    if (title){
        xmlNewChild(author_node, NULL, BAD_CAST "title", BAD_CAST title);
    }
    if (logo_url){
        xmlNewChild(author_node, NULL, BAD_CAST "logoURL", BAD_CAST logo_url);
    }
    if (contact_name){
        xmlNewChild(author_node, NULL, BAD_CAST "contactName", BAD_CAST contact_name);
    }
    if (contact_email){
        xmlNewChild(author_node, NULL, BAD_CAST "contactEmail", BAD_CAST contact_email);
    }
    if (contact_phone){
        xmlNewChild(author_node, NULL, BAD_CAST "contactPhone", BAD_CAST contact_phone);
    }
    if (contributor){
        xmlNewChild(author_node, NULL, BAD_CAST "contributor", BAD_CAST contributor);
    }

    return 0;
}

/**
 * @brief      Fill a VOEvent_Param structure
 *
 * @param[in]  param             VOEvent_Param structure to be filled (cannot be NULL)
 * @param[in]  param_name        Parameter name (cannot be NULL)
 * @param[in]  param_value       Parameter value (cannot be NULL)
 * @param[in]  param_unit        Parameter unit (can be NULL)
 * @param[in]  param_ucd         Parameter UCD (can be NULL)
 * @param[in]  param_datatype    Parameter data type (can be NULL)
 * @param[in]  param_utype       Parameter utype (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_param(struct VOEvent_Param *param, const char* param_name,
    const char* param_value, const char* param_unit, const char* param_ucd,
    const char* param_datatype, const char* param_utype){

    if (param == NULL){
        fprintf(stderr, "Error: Param structure cannot be NULL.\n");
        return 1;
    }

    if (param_name == NULL){
        fprintf(stderr, "Error: specify name of the parameter.\n");
        return 1;
    }

    if (param_value == NULL){
        fprintf(stderr, "Error: specify value of the parameter.\n");
        return 1;
    }

    param->name  = xmlCharStrndup(param_name, strlen(param_name));
    param->value = xmlCharStrndup(param_value, strlen(param_value));

    if (param_unit){
        param->unit = xmlCharStrndup(param_unit, strlen(param_unit));
    }
    if (param->ucd){
        param->ucd = xmlCharStrndup(param_ucd, strlen(param_ucd));
    }
    if (param->dataType){
        param->dataType = xmlCharStrndup(param_datatype, strlen(param_datatype));
    }
    if (param->utype){
        param->utype = xmlCharStrndup(param_utype, strlen(param_utype));
    }

    return 0;

}

/**
 * @brief      Add Parameter to a Group element under What element
 *
 * If the node is not the What element, the function returns. The function also
 * returns if:
 * - the group name is not specified (although the VOEvent schema allows Groups
 * to not have names, here Groups are forced to have names)
 * - the parameter name is not specified (although the VOEvent schema allows
 * parameters to not have names, here they are forced to have names)
 * - the parameter value is not specified (again, not mandatory in the VOEvent
 * schema, but here it is forced to be there)
 *
 * @param[in]  what_node         What element node (cannot be NULL)
 * @param[in]  group_name        Name of Group to which Parameter has to be added (cannot be NULL)
 * @param[in]  group_type        Group type (can be NULL)
 * @param[in]  param_name        Parameter name (cannot be NULL)
 * @param[in]  param_value       Parameter value (cannot be NULL)
 * @param[in]  param_unit        Parameter unit (can be NULL)
 * @param[in]  param_ucd         Parameter UCD (can be NULL)
 * @param[in]  param_datatype    Parameter data type (can be NULL)
 * @param[in]  param_utype       Parameter utype (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_param_to_group(
    xmlNodePtr what_node, const char* group_name, const char* group_type,
    const char* param_name, const char* param_value, const char* param_unit,
    const char* param_ucd, const char* param_datatype, const char* param_utype){

    if (what_node == NULL){
        fprintf(stderr, "Error: What element node cannot be NULL.\n");
        return 1;
    }

    if (xmlStrcmp(what_node->name, (const xmlChar *)"What")){
        fprintf(stderr, "Error: trying to add Author element outside What element.\n");
        return 1;
    }

    if (group_name == NULL){
        fprintf(stderr, "Error: specify name of the group.\n");
        return 1;
    }

    if (param_name == NULL){
        fprintf(stderr, "Error: specify name of the parameter.\n");
        return 1;
    }

    if (param_value == NULL){
        fprintf(stderr, "Error: specify value of the parameter.\n");
        return 1;
    }

    // Try to find group with name group_name

    xmlNodePtr cur = NULL;
    cur = what_node->xmlChildrenNode;

    while (cur != NULL){

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Group"))){
            xmlChar* name = xmlGetProp(cur, (const xmlChar *)"name");
            if ((!xmlStrcmp(name, (const xmlChar *)group_name))){
                xmlFree(name);
                break;
            }
            xmlFree(name);
        }

        cur = cur->next;
    }

    // If no group with name group_name was found, create it
    xmlNodePtr group_node = NULL;

    if (cur == NULL){
        group_node = xmlNewChild(what_node, NULL, BAD_CAST "Group", NULL);
        xmlNewProp(group_node, BAD_CAST "name", BAD_CAST group_name);
        if (group_type){
            xmlNewProp(group_node, BAD_CAST "type", BAD_CAST group_type);
        }
    }
    else{
        group_node = cur;
    }

    xmlNodePtr param_node;

    param_node = xmlNewChild(group_node, NULL, BAD_CAST "Param", NULL);
    xmlNewProp(param_node, BAD_CAST "name", BAD_CAST param_name);
    xmlNewProp(param_node, BAD_CAST "value", BAD_CAST param_value);
    if (param_unit){
        xmlNewProp(param_node, BAD_CAST "unit", BAD_CAST param_unit);
    }
    if (param_ucd){
        xmlNewProp(param_node, BAD_CAST "ucd", BAD_CAST param_ucd);
    }
    if (param_datatype){
        xmlNewProp(param_node, BAD_CAST "dataType", BAD_CAST param_datatype);
    }
    if (param_utype){
        xmlNewProp(param_node, BAD_CAST "utype", BAD_CAST param_utype);
    }

    return 0;
}

/**
 * @brief      Adds more Parameter elements to Group at once
 *
 * @param[in]  what_node     What element node (cannot be NULL)
 * @param[in]  group_name    Name of Group to which Parameter has to be added (cannot be NULL)
 * @param[in]  group_type    Group type (can be NULL)
 * @param[in]  Params        Array of struct VOEvent_Param (cannot be NULL)
 * @param[in]  nr_params     Number of parameters to add (cannot be less than 2)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_params_to_group(
    xmlNodePtr what_node, const char* group_name, const char* group_type,
    const struct VOEvent_Param *Params, const unsigned int nr_params){

    if (what_node == NULL){
        fprintf(stderr, "Error: What element node cannot be NULL.\n");
        return 1;
    }

    if (xmlStrcmp(what_node->name, (const xmlChar *)"What")){
        fprintf(stderr, "Error: trying to add Author element outside What element.\n");
        return 1;
    }

    if (group_name == NULL){
        fprintf(stderr, "Error: specify name of the group.\n");
        return 1;
    }

    if (Params == NULL){
        fprintf(stderr, "Error: specify the array of Parameters.\n");
        return 1;
    }

    if (nr_params < 2){
        fprintf(stderr, "Error: the number of parameters to add must be more than one.\n");
        return 1;
    }

    // Try to find group with name group_name

    xmlNodePtr cur = NULL;
    cur = what_node->xmlChildrenNode;

    while (cur != NULL){

        if ((!xmlStrcmp(cur->name, (const xmlChar *)"Group"))){
            xmlChar* name = xmlGetProp(cur, (const xmlChar *)"name");
            if ((!xmlStrcmp(name, (const xmlChar *)group_name))){
                xmlFree(name);
                break;
            }
            xmlFree(name);
        }

        cur = cur->next;
    }

    // If no group with name group_name was found, create it
    xmlNodePtr group_node = NULL;

    if (cur == NULL){
        group_node = xmlNewChild(what_node, NULL, BAD_CAST "Group", NULL);
        xmlNewProp(group_node, BAD_CAST "name", BAD_CAST group_name);
        if (group_type){
            xmlNewProp(group_node, BAD_CAST "type", BAD_CAST group_type);
        }
    }
    else{
        group_node = cur;
    }

    xmlNodePtr param_node;

    unsigned int params_counter;

    for (params_counter = 0; params_counter < nr_params; ++params_counter){
        param_node = xmlNewChild(group_node, NULL, BAD_CAST "Param", NULL);
        xmlNewProp(param_node, BAD_CAST "name", BAD_CAST Params[params_counter].name);
        xmlNewProp(param_node, BAD_CAST "value", BAD_CAST Params[params_counter].value);
        if (Params[params_counter].unit){
            xmlNewProp(param_node, BAD_CAST "unit", BAD_CAST Params[params_counter].unit);
        }
        if (Params[params_counter].ucd){
            xmlNewProp(param_node, BAD_CAST "ucd", BAD_CAST Params[params_counter].ucd);
        }
        if (Params[params_counter].dataType){
            xmlNewProp(param_node, BAD_CAST "dataType", BAD_CAST Params[params_counter].dataType);
        }
        if (Params[params_counter].utype){
            xmlNewProp(param_node, BAD_CAST "utype", BAD_CAST Params[params_counter].utype);
        }
    }

    return 0;
}

/**
 * @brief      Adds Parameter element to What element
 *
 * @param[in]  what_node         What element node (cannot be NULL)
 * @param[in]  param_name        Parameter name (cannot be NULL)
 * @param[in]  param_value       Parameter value (cannot be NULL)
 * @param[in]  param_unit        Parameter unit (can be NULL)
 * @param[in]  param_ucd         Parameter UCD (can be NULL)
 * @param[in]  param_datatype    Parameter data type (can be NULL)
 * @param[in]  param_utype       Parameter utype (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_param_to_what(
    xmlNodePtr what_node, const char* param_name, const char* param_value,
    const char* param_unit, const char* param_ucd, const char* param_datatype,
    const char* param_utype){

    if (what_node == NULL){
        fprintf(stderr, "Error: What element node cannot be NULL.\n");
        return 1;
    }

    if (xmlStrcmp(what_node->name, (const xmlChar *)"What")){
        fprintf(stderr, "Error: trying to add Author element outside What element.\n");
        return 1;
    }

    if (param_name == NULL){
        fprintf(stderr, "Error: specify name of the parameter.\n");
        return 1;
    }

    if (param_value == NULL){
        fprintf(stderr, "Error: specify value of the parameter.\n");
        return 1;
    }

    xmlNodePtr param_node;

    param_node = xmlNewChild(what_node, NULL, BAD_CAST "Param", NULL);
    xmlNewProp(param_node, BAD_CAST "name", BAD_CAST param_name);
    xmlNewProp(param_node, BAD_CAST "value", BAD_CAST param_value);
    if (param_unit){
        xmlNewProp(param_node, BAD_CAST "unit", BAD_CAST param_unit);
    }
    if (param_ucd){
        xmlNewProp(param_node, BAD_CAST "ucd", BAD_CAST param_ucd);
    }
    if (param_datatype){
        xmlNewProp(param_node, BAD_CAST "dataType", BAD_CAST param_datatype);
    }
    if (param_utype){
        xmlNewProp(param_node, BAD_CAST "utype", BAD_CAST param_utype);
    }

    return 0;
}

/**
 * @brief      Add ObservatoryLocation element to ObsDataLocation element
 *
 *  For the ObservatoryLocation ID, one can use the values from the ::observatory_locations
 *  array and the ::observatory_locations_values enum for the index, to retrieve valid
 *  IDs. Otherwise, the actual name of the observatory can be used, provided by the user.
 *
 * @param[in]  obsdata_node     ObsDataLocation element node (cannot be NULL)
 * @param[in]  id               ObservatoryLocation element ID (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_observatory_location(xmlNodePtr obsdata_node, const char* id){
    if (obsdata_node == NULL){
        fprintf(stderr, "Error: ObsDataLocation element node cannot be NULL.\n");
        return 1;
    }
    xmlNodePtr observatory_location_node = xmlNewChild(obsdata_node, NULL, BAD_CAST "ObservatoryLocation", NULL);
    if (observatory_location_node == NULL){
        fprintf(stderr, "Error in building ObservatoryLocation element.\n");
        return 1;
    }
    if (id){
        xmlNewProp(observatory_location_node, BAD_CAST "id", BAD_CAST id);
    }
    return 0;
}

/**
 * @brief      Add AstroCoordSystem, AstroCoords, Time and Position2D elements to ObsDataLocation element
 *
 * @param[in]  obsdata_node     ObsDataLocation element node (cannot be NULL)
 * @param[in]  coord_id         Space-time coordinates ID (values from ::astro_coord_systems_values enum)
 * @param[in]  isotime          Struct tm containing UTC time info (can be NULL)
 * @param[in]  time_error       Error on time
 * @param[in]  pos2d            VOEvent_Position2D structure containing coordinates info (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_astro_coords_2d(
    xmlNodePtr obsdata_node, const int coord_id, const struct tm *isotime,
    const float time_error, const struct VOEvent_Position2D *pos2d){

    if (obsdata_node == NULL){
        fprintf(stderr, "Error: ObsDataLocation element node cannot be NULL.\n");
        return 1;
    }

    if (coord_id < 0 || coord_id > MAX_ASTRO_COORD_SYSTEMS - 1){
        fprintf(stderr, "Error: coord_id value not recognized.\n");
        return 1;
    }

    if (pos2d == NULL){
        fprintf(stderr, "Error: VOEvent_Position2D structure cannot be NULL.\n");
        return 1;
    }

    xmlNodePtr observation_location_node = xmlNewChild(obsdata_node, NULL, BAD_CAST "ObservationLocation", NULL);
    
    xmlNodePtr astrocoord_system_node = xmlNewChild(observation_location_node, NULL, BAD_CAST "AstroCoordSystem", NULL);
    xmlNewProp(astrocoord_system_node, BAD_CAST "id", BAD_CAST astro_coord_systems[coord_id]);

    xmlNodePtr astrocoords_node = xmlNewChild(observation_location_node, NULL, BAD_CAST "AstroCoords", NULL);
    xmlNewProp(astrocoords_node, BAD_CAST "coord_system_id", BAD_CAST astro_coord_systems[coord_id]);

    //xmlNodePtr time_node = xmlNewChild(astrocoords_node, NULL, BAD_CAST "Time", NULL);
    if (isotime){
        add_time(astrocoords_node, isotime, time_error);
    }

    add_position_2d(astrocoords_node, pos2d);

    return 0;
}

/**
 * @brief      Add AstroCoordSystem, AstroCoords, Time and Position3D elements to ObsDataLocation element
 *
 * @param[in]  obsdata_node     ObsDataLocation element node (cannot be NULL)
 * @param[in]  coord_id         Space-time coordinates ID (values from ::astro_coord_systems_values enum)
 * @param[in]  isotime          Struct tm containing UTC time info (can be NULL)
 * @param[in]  time_error       Error on time
 * @param[in]  pos3d            VOEvent_Position3D structure containing coordinates info (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_astro_coords_3d(
    xmlNodePtr obsdata_node, const int coord_id, const struct tm *isotime,
    const float time_error, const struct VOEvent_Position3D *pos3d){

    if (obsdata_node == NULL){
        fprintf(stderr, "Error: ObsDataLocation element node cannot be NULL.\n");
        return 1;
    }

    if (coord_id < 0 || coord_id > MAX_ASTRO_COORD_SYSTEMS - 1){
        fprintf(stderr, "Error: coord_id value not recognized.\n");
        return 1;
    }

    if (pos3d == NULL){
        fprintf(stderr, "Error: VOEvent_Position3D structure cannot be NULL.\n");
        return 1;
    }

    xmlNodePtr observation_location_node = xmlNewChild(obsdata_node, NULL, BAD_CAST "ObservationLocation", NULL);
    
    xmlNodePtr astrocoord_system_node = xmlNewChild(observation_location_node, NULL, BAD_CAST "AstroCoordSystem", NULL);
    xmlNewProp(astrocoord_system_node, BAD_CAST "id", BAD_CAST astro_coord_systems[coord_id]);

    xmlNodePtr astrocoords_node = xmlNewChild(observation_location_node, NULL, BAD_CAST "AstroCoords", NULL);
    xmlNewProp(astrocoords_node, BAD_CAST "coord_system_id", BAD_CAST astro_coord_systems[coord_id]);

    //xmlNodePtr time_node = xmlNewChild(astrocoords_node, NULL, BAD_CAST "Time", NULL);
    if (isotime){
        add_time(astrocoords_node, isotime, time_error);
    }

    add_position_3d(astrocoords_node, pos3d);

    return 0;
}

/**
 * @brief     Fill a VOEvent_Position2D structure.
 *
 * @param[in]  pos2d          VOEvent_Position2D element node (cannot be NULL)
 * @param[in]  coord1         Coordinate 1 value
 * @param[in]  coord1_name    Coordinate 1 name (cannot be NULL)
 * @param[in]  coord2         Coordinate 2 value
 * @param[in]  coord2_name    Coordinate 2 name (cannot be NULL)
 * @param[in]  unit           Coordinates unit (cannot be NULL)
 * @param[in]  error          Coordinates error
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_position_2d(
    struct VOEvent_Position2D *pos2d, const float coord1, const char* coord1_name,
    const float coord2, const char* coord2_name, const char* unit, const float error){

    if (pos2d == NULL){
        fprintf(stderr, "Error: VOEvent_Position2D structure cannot be NULL.\n");
        return 1;
    }

    if (coord1_name == NULL){
        fprintf(stderr, "Error: name of first coordinate cannot be NULL.\n");
        return 1;
    }

    if (coord2_name == NULL){
        fprintf(stderr, "Error: name of second coordinate cannot be NULL.\n");
        return 1;
    }

    if (unit == NULL){
        fprintf(stderr, "Error: unit cannot be NULL.\n");
        return 1;
    }

    char buffer[24];

    sprintf(buffer, "%f", coord1);
    pos2d->C1 = xmlCharStrndup(buffer, strlen(buffer));
    sprintf(buffer, "%f", coord2);
    pos2d->C2 = xmlCharStrndup(buffer, strlen(buffer));
    sprintf(buffer, "%f", error);
    pos2d->ErrorRadius = xmlCharStrndup(buffer, strlen(buffer));
    pos2d->unit = xmlCharStrndup(unit, strlen(unit));
    pos2d->Name1 = xmlCharStrndup(coord1_name, strlen(coord1_name));
    pos2d->Name2 = xmlCharStrndup(coord2_name, strlen(coord2_name));

    return 0;
}

/**
 * @brief     Add a Position2D element to AstroCoords element.
 *
 * @param[in]  astrocoords_node    AstroCoords element node (cannot be NULL)
 * @param[in]  pos2d               VOEvent_Position2D structure (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_position_2d(xmlNodePtr astrocoords_node, const struct VOEvent_Position2D *pos2d){
    if (astrocoords_node == NULL){
        fprintf(stderr, "Error: AstroCoords element node cannot be NULL.\n");
        return 1;
    }
    if (pos2d == NULL){
        fprintf(stderr, "Error: VOEvent_Position2D structure cannot be NULL.\n");
        return 1;
    }
    xmlNodePtr pos2d_node = xmlNewChild(astrocoords_node, NULL, BAD_CAST "Position2D", NULL);
    xmlNewProp(pos2d_node, BAD_CAST "unit", pos2d->unit);
    xmlNewChild(pos2d_node, NULL, BAD_CAST "Name1", pos2d->Name1);
    xmlNewChild(pos2d_node, NULL, BAD_CAST "Name2", pos2d->Name2);
    xmlNodePtr value2_node = xmlNewChild(pos2d_node, NULL, BAD_CAST "Value2", NULL);
    xmlNewChild(value2_node, NULL, BAD_CAST "C1", pos2d->C1);
    xmlNewChild(value2_node, NULL, BAD_CAST "C2", pos2d->C2);
    xmlNewChild(pos2d_node, NULL, BAD_CAST "Error2Radius", pos2d->ErrorRadius);
    return 0;
}

/**
 * @brief     Fill a VOEvent_Position3D structure.
 *
 * @param[in]  pos3d          ObsDataLocation element node (cannot be NULL)
 * @param[in]  coord1         Coordinate 1 value
 * @param[in]  coord1_name    Coordinate 1 name (cannot be NULL)
 * @param[in]  coord2         Coordinate 2 value
 * @param[in]  coord2_name    Coordinate 2 name (cannot be NULL)
 * @param[in]  coord3         Coordinate 3 value
 * @param[in]  coord3_name    Coordinate 3 name (cannot be NULL)
 * @param[in]  unit           Coordinates unit (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_position_3d(
    struct VOEvent_Position3D *pos3d, const float coord1, const char* coord1_name,
    const float coord2, const char* coord2_name, const float coord3,
    const char* coord3_name, const char* unit){

    if (pos3d == NULL){
        fprintf(stderr, "Error: VOEvent_Position3D structure cannot be NULL.\n");
        return 1;
    }

    if (coord1_name == NULL){
        fprintf(stderr, "Error: name of first coordinate cannot be NULL.\n");
        return 1;
    }

    if (coord2_name == NULL){
        fprintf(stderr, "Error: name of second coordinate cannot be NULL.\n");
        return 1;
    }

    if (coord3_name == NULL){
        fprintf(stderr, "Error: name of third coordinate cannot be NULL.\n");
        return 1;
    }

    if (unit == NULL){
        fprintf(stderr, "Error: unit cannot be NULL.\n");
        return 1;
    }

    char buffer[24];

    sprintf(buffer, "%f", coord1);
    pos3d->C1 = xmlCharStrndup(buffer, strlen(buffer));
    sprintf(buffer, "%f", coord2);
    pos3d->C2 = xmlCharStrndup(buffer, strlen(buffer));
    sprintf(buffer, "%f", coord3);
    pos3d->C3 = xmlCharStrndup(buffer, strlen(buffer));
    pos3d->unit = xmlCharStrndup(unit, strlen(unit));
    pos3d->Name1 = xmlCharStrndup(coord1_name, strlen(coord1_name));
    pos3d->Name2 = xmlCharStrndup(coord2_name, strlen(coord2_name));
    pos3d->Name3 = xmlCharStrndup(coord3_name, strlen(coord3_name));

    return 0;
}

/**
 * @brief     Add a Position3D element to AstroCoords element.
 *
 * @param[in]  astrocoords_node    AstroCoords element node (cannot be NULL)
 * @param[in]  pos3d               VOEvent_Position3D structure (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_position_3d(xmlNodePtr astrocoords_node, const struct VOEvent_Position3D *pos3d){
    if (astrocoords_node == NULL){
        fprintf(stderr, "Error: AstroCoords element node cannot be NULL.\n");
        return 1;
    }
    if (pos3d == NULL){
        fprintf(stderr, "Error: VOEvent_Position3D structure cannot be NULL.\n");
        return 1;
    }
    xmlNodePtr pos3d_node = xmlNewChild(astrocoords_node, NULL, BAD_CAST "Position3D", NULL);
    xmlNewProp(pos3d_node, BAD_CAST "unit", pos3d->unit);
    xmlNewChild(pos3d_node, NULL, BAD_CAST "Name1", pos3d->Name1);
    xmlNewChild(pos3d_node, NULL, BAD_CAST "Name2", pos3d->Name2);
    xmlNewChild(pos3d_node, NULL, BAD_CAST "Name3", pos3d->Name3);
    xmlNodePtr value3_node = xmlNewChild(pos3d_node, NULL, BAD_CAST "Value3", NULL);
    xmlNewChild(value3_node, NULL, BAD_CAST "C1", pos3d->C1);
    xmlNewChild(value3_node, NULL, BAD_CAST "C2", pos3d->C2);
    xmlNewChild(value3_node, NULL, BAD_CAST "C3", pos3d->C3);
    return 0;
}

/**
 * @brief     Add a Time, TimeInstant, ISOTime and Error elements to AstroCoords element.
 *
 * @param[in]  astrocoords_node    AstroCoords element node (cannot be NULL)
 * @param[in]  isotime             Struct tm containing the time (cannot be NULL)
 * @param[in]  time_error          Error on time
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_time(xmlNodePtr astrocoords_node, const struct tm *isotime, const float time_error){
    if (astrocoords_node == NULL){
        fprintf(stderr, "Error: AstroCoords element node cannot be NULL.\n");
        return 1;
    }
    if (isotime == NULL){
        fprintf(stderr, "Error: the tm structure cannot be NULL.\n");
        return 1;
    }
    char datebuf[30];
    strftime(datebuf, 30, "%FT%T", isotime);
    xmlNodePtr time_node = xmlNewChild(astrocoords_node, NULL, BAD_CAST "Time", NULL);
    xmlNodePtr time_instant_node = xmlNewChild(time_node, NULL, BAD_CAST "TimeInstant", NULL);
    xmlNewChild(time_instant_node, NULL, BAD_CAST "ISOTime", BAD_CAST datebuf);
    sprintf(datebuf, "%f", time_error);
    xmlNewChild(time_node, NULL, BAD_CAST "Error", BAD_CAST datebuf);
    return 0;
}

/**
 * @brief     Fill a VOEvent_Reference structure.
 *
 * @param[in]  reference    AstroCoords element node (cannot be NULL)
 * @param[in]  uri          Reference uri (cannot be NULL)
 * @param[in]  type         Reference type (can be NULL)
 * @param[in]  mimetype     Reference mimetype (can be NULL)
 * @param[in]  meaning      Reference meaning (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_reference(
    struct VOEvent_Reference* reference, const char* uri,
    const char* type, const char* mimetype, const char* meaning){

    if (reference == NULL){
        fprintf(stderr, "Error: trying to pass a NULL VOEvent_Reference.\n");
        return 1;
    }

    if (uri == NULL){
        fprintf(stderr, "Error: provide at least URI for Reference elements.\n");
        return 1;
    }

    reference->uri = xmlCharStrndup(uri, strlen(uri));

    if (type){
        reference->type = xmlCharStrndup(type, strlen(type));
    }
    if (mimetype){
        reference->mimetype = xmlCharStrndup(mimetype, strlen(mimetype));
    }
    if (meaning){
        reference->meaning = xmlCharStrndup(meaning, strlen(meaning));
    }

    return 0;
}

/**
 * @brief     Add a Reference element to another element
 *
 * @param[in]  node         Element node to which add Reference (cannot be NULL)
 * @param[in]  reference    VOEvent_Reference structure (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_reference(xmlNodePtr node, const struct VOEvent_Reference* reference){

    if (node == NULL){
        fprintf(stderr, "Error: passing a NULL node pointer.\n");
        return 1;
    }

    if (reference == NULL){
        fprintf(stderr, "Error: passing a VOEvent_Reference pointer.\n");
        return 1;
    }

    if (reference->uri == NULL){
        fprintf(stderr, "Error: provide at least URI for Reference elements.\n");
        return 1;
    }

    xmlNodePtr ref_node = xmlNewChild(node, NULL, BAD_CAST "Reference", NULL);
    xmlNewProp(ref_node, BAD_CAST "uri", BAD_CAST reference->uri);
    if (reference->type){
        xmlNewProp(ref_node, BAD_CAST "type", BAD_CAST reference->type);
    }
    if (reference->mimetype){
        xmlNewProp(ref_node, BAD_CAST "mimetype", BAD_CAST reference->mimetype);
    }
    if (reference->meaning){
        xmlNewProp(ref_node, BAD_CAST "meaning", BAD_CAST reference->meaning);
    }
    return 0;
}

/**
 * @brief     Fill a VOEvent_Description structure.
 *
 * @param[in]  description  VOEvent_Description structure (cannot be NULL)
 * @param[in]  text         The description text (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_description(struct VOEvent_Description* description, const char* text){

    if (description == NULL){
        fprintf(stderr, "Error: trying to pass a NULL VOEvent_Description.\n");
        return 1;
    }

    if (text == NULL){
        fprintf(stderr, "Error: provide a non NULL string.\n");
        return 1;
    }

    description->description = xmlCharStrndup(text, strlen(text));

    return 0;
}

/**
 * @brief     Add a Description element to another element
 *
 * @param[in]  node           Element node to which add Description (cannot be NULL)
 * @param[in]  description    VOEvent_Description structure (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_description(xmlNodePtr node, const struct VOEvent_Description* description){
    if (node == NULL){
        fprintf(stderr, "Error: passing a NULL node pointer.\n");
        return 1;
    }

    if (description == NULL){
        fprintf(stderr, "Error: passing a NULL VOEvent_Description pointer.\n");
        return 1;
    }

    if (description->description == NULL){
        fprintf(stderr, "Error: description text should be not NULL.\n");
        return 1;
    }

    xmlNodePtr descr_node = xmlNewChild(node, NULL, BAD_CAST "Description", BAD_CAST description->description);
    if (descr_node == NULL){
        fprintf(stderr, "Error: cannot build Description element.\n");
        return 1;
    }
    return 0;
}

/**
 * @brief     Add a EventIVORN element to Citations element
 *
 * @param[in]  citations_node    Citations node (cannot be NULL)
 * @param[in]  event_ivorn       Event IVORN (cannot be NULL)
 * @param[in]  cite              Cite attribute of Event IVORN (value from ::cites_values enum)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_event_ivorn(xmlNodePtr citations_node, const char* event_ivorn, const int cite){

    if (citations_node == NULL){
        fprintf(stderr, "Error: Citations element node cannot be NULL.\n");
        return 1;
    }

    if (cite < 0 || cite > MAX_CITES - 1){
        fprintf(stderr, "Error: cite value not recognized.\n");
        return 1;
    }

    if (event_ivorn == NULL){
        fprintf(stderr, "Error: trying to pass a NULL event IVORN.\n");
        return 1;
    }

    xmlNodePtr event_ivorn_node = xmlNewChild(citations_node, NULL, BAD_CAST "EventIVORN", BAD_CAST event_ivorn);
    if (event_ivorn_node == NULL){
        fprintf(stderr, "Error: cannot build EventIVORN element.\n");
        return 1;
    }
    xmlNewProp(event_ivorn_node, BAD_CAST "cite", BAD_CAST cites[cite]);
    return 0;
}

/**
 * @brief     Fill a VOEvent_Inference structure.
 *
 * The structures VOEvent_Name and VOEvent_Concept contained in
 * the VOEvent_Inference structure are allocated within the function,
 * no need to do it outside (or, if you do, it will not be allocated
 * again).
 *
 * @param[in]  inference    VOEvent_Inference structure (cannot be NULL)
 * @param[in]  probability  Probability value
 * @param[in]  relation     Relation
 * @param[in]  name         Name of inference (can be NULL)
 * @param[in]  concept      Concept (can be NULL)
 * @param[in]  description  VOEvent_Description structure (can be NULL)
 * @param[in]  reference    VOEvent_Reference structure (can be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int fill_inference(
    struct VOEvent_Inference* inference, const float probability,
    const char* relation, const char* name, const char* concept,
    const struct VOEvent_Description* description,
    const struct VOEvent_Reference* reference){

    if (inference == NULL){
        fprintf(stderr, "Error: trying to pass a NULL VOEvent_Inference.\n");
        return 1;
    }

    if (name){
        if (inference->Name == NULL){
            inference->Name = (struct VOEvent_Name *) malloc(sizeof(struct VOEvent_Name));
            memset(inference->Name, 0, sizeof(struct VOEvent_Name));
        }
        inference->Name->name = xmlCharStrndup(name, strlen(name));
        inference->nr_name++;
    }

    if (concept){
        if (inference->Concept == NULL){
            inference->Concept = (struct VOEvent_Concept *) malloc(sizeof(struct VOEvent_Concept));
            memset(inference->Concept, 0, sizeof(struct VOEvent_Concept));
        }
        inference->Concept->concept = xmlCharStrndup(concept, strlen(concept));
        inference->nr_concept++;
    }

    if (description){
        inference->Description = (struct VOEvent_Description *) description;
        inference->nr_description++;
    }

    if (reference){
        inference->Reference = (struct VOEvent_Reference *) reference;
        inference->nr_reference++;
    }

    char buffer[24];
    sprintf(buffer, "%f", probability);
    inference->probability = xmlCharStrndup(buffer, strlen(buffer));

    if (relation){
        inference->relation = xmlCharStrndup(relation, strlen(relation));
    }

    return 0;
}

/**
 * @brief     Add an Inference element to a Why element
 *
 * @param[in]  why_node   Why node to which add inference (cannot be NULL)
 * @param[in]  inference  VOEvent_Inference structure (cannot be NULL)
 *
 * @return     0 if no errors occurred, 1 otherwise
 */
int add_inference(xmlNodePtr why_node, const struct VOEvent_Inference* inference){
    if (why_node == NULL){
        fprintf(stderr, "Error: passing a NULL node pointer.\n");
        return 1;
    }

    if (inference == NULL){
        fprintf(stderr, "Error: passing a NULL VOEvent_Inference pointer.\n");
        return 1;
    }

    if (xmlStrcmp(why_node->name, (const xmlChar *)"Why")){
        fprintf(stderr, "Error: trying to add Inference element outside Why element.\n");
        return 1;
    }

    xmlNodePtr inference_node = xmlNewChild(why_node, NULL, BAD_CAST "Inference", NULL);
    if (inference_node == NULL){
        fprintf(stderr, "Error: cannot build Inference element.\n");
        return 1;
    }

    if (inference->probability){
        xmlNewProp(inference_node, BAD_CAST "probability", inference->probability);
    }

    if (inference->relation){
        xmlNewProp(inference_node, BAD_CAST "relation", inference->relation);
    }

    if (inference->Name){
        xmlNewChild(inference_node, NULL, BAD_CAST "Name", inference->Name->name);
    }
    if (inference->Concept){
        xmlNewChild(inference_node, NULL, BAD_CAST "Concept", inference->Concept->concept);
    }

    int status = 0;

    if (inference->Description){
        status = add_description(inference_node, inference->Description);
        if (status){
            fprintf(stderr, "Error: cannot add Description element to Inference element.\n");
            return status;
        }
    }
    if (inference->Reference){
        status = add_reference(inference_node, inference->Reference);
        if (status){
            fprintf(stderr, "Error: cannot add Reference element to Inference element.\n");
            return status;
        }
    }

    return 0;
}
