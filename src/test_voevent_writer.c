/**
 * @file test_voevent_writer.c
 * @brief Source file to test the voevent reader/parser.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "voevent.h"

int main(int argc, char const *argv[])
{
    if(argc < 1)
    {
        fprintf(stderr, "You did not provide enough arguments. Exiting.\n");
        return 1;
    }

    /*
     * This initializes the library and check potential ABI mismatches
     * between the version it was compiled for and the actual shared
     * library used. Calls xmlInitParser() internally
    */
    LIBXML_TEST_VERSION

    // create XML document pointer
    xmlDocPtr doc = create_doc();

    // create root (VOEvent) node
    xmlNodePtr voevent_root_node = create_root_node(doc, "ivo://test/Test#VOEvent-test", TEST);
    // create Who element
    xmlNodePtr voevent_who_node = create_who(voevent_root_node, NULL, "ivo://test.voevent/test" , NULL, NULL);
    int status = 0;
    status = add_author(voevent_who_node, "voevent-c-parser", NULL, NULL, "Alessio Berti", "alessioberti90@gmail.com", NULL, NULL);
    if (status){
        xmlFreeDoc(doc);
        xmlCleanupParser();
        return 1;
    }

    // create a description structure
    struct VOEvent_Description* description_who = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));
    memset(description_who, 0, sizeof(struct VOEvent_Description));
    status = fill_description(description_who, "This VOEvent message was created by voevent-c-parser.");
    if (status){
        fprintf(stderr, "Could not fill description.\n");
    }
    // add a Description element to Who element
    status = add_description(voevent_who_node, description_who);
    if (status){
        fprintf(stderr, "Could not add description to Who element.\n");
    }

    // create What element
    xmlNodePtr voevent_what_node = create_what(voevent_root_node, NULL, NULL);
    status = add_param_to_what(voevent_what_node, "Significance", "23.5", "sigma", "stat.snr", NULL, NULL);
    if (status){
        fprintf(stderr, "Error adding parameter to What element.\n");
    }

    int nr_params = 3;
    struct VOEvent_Param* params_group = (struct VOEvent_Param *) malloc(nr_params*sizeof(struct VOEvent_Param));
    memset(params_group, 0, nr_params*sizeof(struct VOEvent_Param));

    status = fill_param(&params_group[0], "Flux", "5e-4", "erg/cm2/s", NULL, NULL, NULL);
    if (status){
        fprintf(stderr, "Error filling parameter info.\n");
    }
    status = fill_param(&params_group[1], "Distance", "40", "Mpc", NULL, NULL, NULL);
    if (status){
        fprintf(stderr, "Error filling parameter info.\n");
    }
    status = fill_param(&params_group[2], "Jet angle", "56", "deg", NULL, NULL, NULL);
    if (status){
        fprintf(stderr, "Error filling parameter info.\n");
    }

    status = add_params_to_group(voevent_what_node, "Misc Info", NULL, params_group, nr_params);
    if (status){
        fprintf(stderr, "Error adding group.\n");
    }

    xmlNodePtr voevent_wherewhen_node = create_wherewhen(voevent_root_node, NULL);
    status = add_observatory_location(voevent_wherewhen_node, "GEOLUN");
    if (status){
        fprintf(stderr, "Error adding observatory location element.\n");
    }

    // create a Position2D element and fill it
    struct VOEvent_Position2D* pos2d = (struct VOEvent_Position2D *) malloc(sizeof(struct VOEvent_Position2D));
    memset(pos2d, 0, sizeof(struct VOEvent_Position2D));
    status = fill_position_2d(pos2d, 234.5, "Right Ascension", -30.1, "Declination", "degrees", 0.05);
    if (status){
        fprintf(stderr, "Error filling Position2D element.\n");
    }

    struct tm *t;
    time_t t_now;

    time(&t_now);
    t = gmtime(&t_now);

    // add AstroCoords element with a Position2D element
    add_astro_coords_2d(voevent_wherewhen_node, UTC_FK5_GEO, t, 0, pos2d);

    // create and Inference element and then fill it
    struct VOEvent_Description* description_inference = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));
    memset(description_inference, 0, sizeof(struct VOEvent_Description));
    status = fill_description(description_inference, "X-ray afterglow");
    if (status){
        fprintf(stderr, "Could not fill description.\n");
    }
    struct VOEvent_Inference* inference = (struct VOEvent_Inference *) malloc(1*sizeof(struct VOEvent_Inference));
    memset(inference, 0, sizeof(struct VOEvent_Inference));
    status = fill_inference(inference, 1.0, "identified", "GRB 170817A", "misc.GRB", description_inference, NULL);
    if (status){
        fprintf(stderr, "Could not fill inference.\n");
    }

    // create a reference structure
    struct VOEvent_Reference* reference_how = (struct VOEvent_Reference *) malloc(sizeof(struct VOEvent_Reference));
    memset(reference_how, 0, sizeof(struct VOEvent_Reference));
    status = fill_reference(reference_how, "https://gitlab.com/aleberti/voevent-c-parser", "url", NULL, NULL);
    if (status){
        fprintf(stderr, "Could not fill reference.\n");
    }
    // create a description structure
    struct VOEvent_Description* description_how = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));
    memset(description_how, 0, sizeof(struct VOEvent_Description));
    status = fill_description(description_how, "voevent-c-parser, a parser and writer for VOEvents.");
    if (status){
        fprintf(stderr, "Could not fill description.\n");
    }
    // create How element, adding a Reference and Description elements
    create_how(voevent_root_node, reference_how, description_how);

    // create Why element and add Inference to it
    create_why(voevent_root_node, 1.0, NULL, inference);

    //create Citations element with a description element
    struct VOEvent_Description* description_citations = (struct VOEvent_Description *) malloc(sizeof(struct VOEvent_Description));
    memset(description_citations, 0, sizeof(struct VOEvent_Description));
    status = fill_description(description_citations, "Follow-up observation of GW170817.");
    if (status){
        fprintf(stderr, "Could not fill description.\n");
    }
    create_citations(voevent_root_node, "ivo://test.voevent/test#GW170817", FOLLOWUP, description_citations);

    // save document, print it to stdout
    xmlSaveFormatFileEnc("-", doc, "UTF-8", 1);
    xmlSaveFormatFileEnc("test_voevent_writer.xml", doc, "UTF-8", 1);

    status = validate_voevent_xmlfile("test_voevent_writer.xml", VOEventSchemaFile);
    if (status){
        fprintf(stderr, "Error validating VOEvent.\n");
    }

    /*free the document */
    xmlFreeDoc(doc);

    int i = 0;
    for (i = 0; i < nr_params; ++i){
       free_voevent_param(&params_group[i]);
    }
    free(params_group);
    free_voevent_description(description_who);
    free(description_who);
    free_voevent_description(description_how);
    free(description_how);
    free_voevent_description(description_citations);
    free(description_citations);
    free_voevent_inference(inference);
    free(inference);
    free_voevent_reference(reference_how);
    free(reference_how);
    free_voevent_position2d(pos2d);
    free(pos2d);

    xmlCleanupParser();

    return 0;
}
